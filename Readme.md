# Ptask evaluation

This repository contains the different source codes to run the experiments of the chapter 5 of my thesis.

## Files

The folder `experiments/g5k` contains the source code to execute the experiment of G5K.
The entry point of the experiment is `/experiments/g5k/main.py`.

The folder `experiments/simgrid` contains the source code, and the necessary files to execute the simulation experiments.

The folder `analysis` contains the data analysis scripts to read the data from the real experiments.

The folder `environments` contains the nix files related to the packaging of the experiments.

- Use (from the root folder of the repo) `nix-shell . -A rEnv` to start a shell with the R dependencies for the .R files.
- Use (from the root folder of the repo) `nix-shell . -A kernelEnv` to start a shell with the python dependencies for the ..ipynb files.

## Run the real Experiment on Grid5000

To create a job, on the rennes frontend:

```bash
oarsub -I -l slash_22=2+{"virtual!='NO' AND cluster='paravance'"},/switch=1/nodes=3,walltime=2  -t deploy
oarsub -I -l slash_22=2+{"virtual!='NO' AND cluster='grisou'"},/switch=1/nodes=3,walltime=2  -t deploy
```

The parameter `-l slash_22=2+{"virtual!='NO' AND cluster='grisou'"}` enables to allocate a subnetwork.

Once the job started:

```bash
kadeploy3 -f $OAR_NODE_FILE -e debian10-x64-base -k
```

The start point for the experiment is the script: `main.py`.
The only dependency is the library PyYAML (3.13).

ssh needs to be configured on your home on the frontend.
It not a good idea to keep the configuration like this at it disable security check.

```sh
cat ~/.ssh/config

Host *
  StrictHostKeyChecking no
```


```
ssh -i experiment_keys/id_rsa -o ProxyCommand="ssh -i experiment_keys/id_rsa -W %h:%p g5k@parasilo-1.rennes.grid5000.fr" g5k@10.158.20.3
```

Trace MPI binding:
Report  `-report-bindings`

