# Experiments

This repo becomes a bit difficult to understand because of the numerous notebooks, source and data it contains.
This file chronologically (later experiments are on top) regroups the different experiments and their entry points (data, notebook, inputs etc).

The hash of the relevant commit is introduced in the title of the experiment.

## BMF

The BMF algorithm has been added to simgrid and available here: https://framagit.org/bruno.donassolo/simgrid/-/branches?state=all&sort=updated_desc&search=bmf.


## Multicore

Lately, Millian reported this error on Simgrid: https://framagit.org/simgrid/simgrid/-/issues/95#note_1105444
(On cpu bound applications, multicore + ptask seems to be wrong, and computes faster than it should)

However, in this project I use ptask+multicore. So, to try the same analysis without multicore, I replayed  the simulations with the communication matrix aggregated by host, instead of mpi ranks.

The file `./experiments/simgrid/number_of_connections.all.csv` contains the new data.
The previous notebook can be used without modificaton with this new dataset (`ptask-eval/analysis/grisous_vs_ptask_connections_number.Rmd`).

The results are a bit different, but it doesn't invalidate the previous analysis concerning the sharing model of ptask being wrong.

## Comparison between reality, Ptask and SMPI whith network interference [fbafc16]

This experiment compares two models of simulation (SMPI and Ptask) with the reality (Grisou).

To generate the data:

```bash
# Go to the folder simgrid
$ cd ptask-eval/experiments/simgrid
# Build the simulation code
$ mkdir -p build && cd build && meson .. && ninja && cd ..
$ ./scripts/generate_data.py
```

This script generates the file: `ptask-eval/experiments/simgrid/number_of_connections.csv` (that should also be committed).

It uses the simgrid program located at `ptask-eval/experiments/simgrid/src/main.cpp`, its input is the `ptask` described in the file: `ptask-eval/experiments/simgrid/profiles/paje_nbhost-32_nbrank-256_dims-80000_subdivisions-50/profile.json`.

### Compare Grisou with ptask for the number of connections

With the shell

```bash
$ pwd
>> ptask-eval/
$ nix-shell . -A rEnv
$ cd analysis
$ rstudio
```

A notebook, to compare the ptask to the Grisou real execution is located at `ptask-eval/analysis/grisous_vs_ptask_connections_number.Rmd` (from the project's root).

## Graphkali [917068a]

With the last experiment it became clear that, the number of connection doesn't have the same impact in Paravance then in Grisou.
To go further, we generate different connection patterns, to have more insight on what is happening.

The final file of this experiment is the notebook located at `./analysis/graph_kali.Rmd`.


## Number of connections [e7e5664]

This experiment aims at understanding the result presented in my thesis: Why grisou and paravance platform are different in respect of the monitored network.

For that, we want to study  the impact of the number of tcp connection on the MPI application.
It is the first attempt to understand why Paravance and Grisou have different results.

The notebook of for this experiment are:
- `./analysis/Grisou_connections_Apr_07_2021.ipynb`
- `./analysis/Paravance_connections_Apr_07_2021.ipynb`

    The input to configure the experiment on g5k are located here:
- ./experiments/g5k/experiments_inputs/experiments_tcp_connections_variation_paravance
- ./experiments/g5k/experiments_inputs/experiments_tcp_connections_variation_grisou

    TODO: compress the data
