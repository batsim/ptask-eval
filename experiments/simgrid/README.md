To generate the data:

```bash
# Go to the folder simgrid
$ cd ptask-eval/experiments/simgrid
# Build the simulation code
$ mkdir -p build && cd build && meson .. && ninja && cd ..
$ ./scripts/generate_data.py
```

This script generates the file: `ptask-eval/experiments/simgrid/number_of_connections.csv`.

It uses the simgrid program located at `ptask-eval/experiments/simgrid/src/main.cpp`, its input is the `ptask` described in the file: `ptask-eval/experiments/simgrid/profiles/paje_nbhost-32_nbrank-256_dims-80000_subdivisions-50/profile.json`.

# Compare Grisou with ptask for the number of connections

With the shell

```bash
$ pwd
>> ptask-eval/
$ nix-shell . -A rEnv
$ cd analysis
$ rstudio
```

A notebook, to compare the ptask to the Grisous real execution is located at `ptask-eval/analysis/grisous_vs_ptask_connections_number.Rmd` (from the project's root).
