#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <simgrid/s4u.hpp>
#include <smpi/smpi.h>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include <dlfcn.h>
#include <stdio.h>

#include "docopt/docopt.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(mwe, "Messages specific for this s4u MWE");


using namespace rapidjson;
using namespace std;
using namespace simgrid;

static void tcpkali_server();

vector<s4u::Host *> read_hostfile(std::string path) {
    vector<s4u::Host *> hosts;

    std::ifstream infile(path);

    std::string hostname;
    while (infile >> hostname)
    {
        hosts.push_back(simgrid::s4u::Host::by_name(hostname));
    }
    return hosts;
}


static void tcpkali_client(simgrid::s4u::ActorPtr server_actor, double period_idle, double period_interf)
{
    XBT_INFO("Tcpkali client");
    std::string mailbox_name_rec  = "mb_tcpkali_1";
    std::string mailbox_name_send = "mb_tcpkali_2";

    simgrid::s4u::Mailbox* mbox_rec  = simgrid::s4u::Mailbox::by_name(mailbox_name_rec);
    simgrid::s4u::Mailbox* mbox_send = simgrid::s4u::Mailbox::by_name(mailbox_name_send);

    if(server_actor != NULL) {
      // mbox_rec->set_receiver(simgrid::s4u::Actor::self());
      // mbox_send->set_receiver(server_actor);
    }

    void *data;

    int *payload = new int;
    *payload = 42;

    while(true) {
        auto comm_rec  = mbox_rec->get_async<void>(&data);
        // Looks like 18446744073709551615U is the max uint64_t
        auto comm_send = mbox_send->put_async((void*) payload,  (uint64_t) 18446744073709551615U);

        XBT_INFO("interference");
        simgrid::s4u::this_actor::sleep_for(period_interf);
        comm_rec->cancel();
        comm_send->cancel();
        XBT_INFO("idle");
        simgrid::s4u::this_actor::sleep_for(period_idle);
    }

    delete payload;
}

static void tcpkali_server()
{
    XBT_INFO("Tcpkali server");
    std::string mailbox_name_rec  = "mb_tcpkali_2";
    std::string mailbox_name_send = "mb_tcpkali_1";

    int *payload = new int;
    *payload = 42;
    void *data;

    while(true) {
        simgrid::s4u::Mailbox* mbox_rec = simgrid::s4u::Mailbox::by_name(mailbox_name_rec);
        auto comm_rec = mbox_rec->get_async<void>(&data);

        simgrid::s4u::Mailbox* mbox_send = simgrid::s4u::Mailbox::by_name(mailbox_name_send);
        auto comm_send = mbox_send->put_async((void*)payload, (uint64_t) 18446744073709551615U);

        try {
            comm_rec->wait();
            comm_send->wait();
        } catch (simgrid::NetworkFailureException &e) {
             XBT_INFO("Stop interferences (this is normal)");
        }

    }

    delete payload;
}

int STOP = false;
static void tcpkali_ptask(std::string server, std::string client, double period_idle, double period_interf)
{
    XBT_INFO("Tcpkali parallel task");
    auto e = s4u::Engine::get_instance();

    vector<s4u::Host *> hosts = { e->host_by_name(client), e->host_by_name(server) };
    std::vector<double> comp = {0, 0};

    const double net_quantity = 1000e10;

    std::vector<double> comm_ser = {0, net_quantity, 0, 0};
    std::vector<double> comm_cli = {0, 0, net_quantity, 0};

    while(!STOP) {
      std::vector<simgrid::s4u::ExecPtr> flows;
      for(int i = 0; i < 1; i++) {
        flows.push_back(simgrid::s4u::this_actor::exec_init(hosts, comp, comm_cli));
        flows.push_back(simgrid::s4u::this_actor::exec_init(hosts, comp, comm_ser));
      }

      for(auto f: flows) {
        f->start();
      }

      XBT_INFO("tcpkali start");
      simgrid::s4u::this_actor::sleep_for(period_interf);

      double total_quantity = 0;
      for(auto f: flows) {
        auto done = (1 - f->get_remaining_ratio());
        total_quantity += net_quantity * done;
        f->cancel();
      }

      XBT_INFO("tcpkali total done: %g", total_quantity);

      XBT_INFO("tcpkali finished");
      simgrid::s4u::this_actor::sleep_for(period_idle);
    }
}

static void tcpkali_ptask_nb_connections(std::string server, std::string client, unsigned int nb_connections)
{
  try {
    XBT_INFO("Tcpkali parallel task");
    auto e = s4u::Engine::get_instance();

    vector<s4u::Host *> hosts = { e->host_by_name(client), e->host_by_name(server) };
    std::vector<double> comp = {0, 0};

    const double net_quantity = 1000e100;

    std::vector<double> comm_ser = {0, net_quantity, 0, 0};
    std::vector<double> comm_cli = {0, 0, net_quantity, 0};

    std::vector<simgrid::s4u::ExecPtr> flows;
    for(unsigned int i = 0; i < nb_connections; i++) {
      for(int j = 0; j < 1; j++) {
        flows.push_back(simgrid::s4u::this_actor::exec_init(hosts, comp, comm_cli));
        flows.push_back(simgrid::s4u::this_actor::exec_init(hosts, comp, comm_ser));
      }
    }
    for(auto f: flows) {
      f->start();
    }

   for(auto f: flows) {
      f->wait();
    }

    // simgrid::s4u::Exec::wait_any(&flows);
  } catch (simgrid::CancelException &e) {
      XBT_INFO("ptask killed, the simulation must be terminated");
  }
}

static void tcpkali_nb_connections(std::string mailbox_name_rec, std::string mailbox_name_send, unsigned int nb_connections)
{
    XBT_INFO("Tcpkali client (using comm)");
    simgrid::s4u::Mailbox* mbox_rec  = simgrid::s4u::Mailbox::by_name(mailbox_name_rec);
    simgrid::s4u::Mailbox* mbox_send = simgrid::s4u::Mailbox::by_name(mailbox_name_send);

    mbox_rec->set_receiver(simgrid::s4u::Actor::self());

    void *data;

    int *payload = new int;
    *payload = 42;

    std::vector<simgrid::s4u::CommPtr> pending_comms;

    for(unsigned int i = 0; i < nb_connections; i++) {
        simgrid::s4u::CommPtr comm_rec  = mbox_rec->get_async<void>(&data);
        simgrid::s4u::CommPtr comm_send = mbox_send->put_async((void*) payload, (uint64_t) 18446744073709551615U);
        pending_comms.push_back(comm_rec);
        pending_comms.push_back(comm_send);
    }

    try {
      simgrid::s4u::Comm::wait_all(pending_comms);
    } catch (simgrid::NetworkFailureException &e) {
      XBT_INFO("Stop interferences (this is normal)");
    }

    delete payload;
}

/**
 * This function executes a parallel task from a json file and a hostfile.
 * The hostfile explicit the mapping between the ranks of the parallel task.
 * The json file holds the communication matrix, it should have the same format as a batsim profile.
 * ```
 * {
 *    "profile": {
 *      "com" : [ 0, 0, 0,
 *                0, 0, 0,
 *                0, 0, 0 ]
 *      "cpu" : [ 0, 0, 0 ]
 *    }
 * }
 * ```
 */
void start_parallel_profile_ptask(Document & d, std::string profile_name, std::string hostfile, double & current, double progress_ratio) {
    auto e = s4u::Engine::get_instance();
    std::vector<double> comm;
    const Value& a = d[profile_name.c_str()]["com"];
    assert(a.IsArray());
    for (SizeType i = 0; i < a.Size(); i++) {
        auto amount = a[i].GetDouble();
        comm.push_back(amount);
    }

    std::vector<double> comp;
    const Value& comp_v = d[profile_name.c_str()]["cpu"];
    assert(a.IsArray());
    for (SizeType i = 0; i < comp_v.Size(); i++) {
        auto amount = comp_v[i].GetDouble();
        comp.push_back(amount);
    }

    auto hosts = read_hostfile(hostfile);

    simgrid::s4u::ExecPtr ptask = simgrid::s4u::this_actor::exec_init(hosts, comp, comm);
    // XBT_INFO("Start parallel task");
    ptask->start();

    // simgrid::s4u::this_actor::sleep_for(1e-9);
    // simgrid::s4u::this_actor::parallel_execute(hosts, comp, comm);

    while(!ptask->test()) {

      simgrid::s4u::this_actor::sleep_for(1);
      auto clock = e->get_clock();
      auto done = (1 - ptask->get_remaining_ratio());
      auto progress  =  done / progress_ratio;
      printf("%f,%f\n", clock, current + progress);
    }

    current += 1 / progress_ratio;

}

void start_parallel_task(std::string profile, std::string hostfile) {
  FILE* fp = fopen(profile.c_str(), "r"); // non-Windows use "r"
  char readBuffer[65536];
  FileReadStream is(fp, readBuffer, sizeof(readBuffer));
  Document d;
  d.ParseStream(is);
  fclose(fp);

  auto profile_name = std::string("profile");
  std::string profile_type = d[profile_name.c_str()]["type"].GetString();

  // output csv
  printf("time,progress\n");

  double progress = 0;
  if(profile_type == "parallel") {
    std::cerr<< "profile type:" << profile_type << std::endl;
    start_parallel_profile_ptask(d, profile_name, hostfile, progress, 1);
  } else if (profile_type == "composed") {
    std::cerr<< "profile type:" << profile_type << std::endl;
    const Value& a = d[profile_name.c_str()]["seq"];
    std::vector<std::string> profiles;
    for (SizeType i = 0; i < a.Size(); i++) {
      std::string seq_profile_name  = a[i].GetString();
      profiles.push_back(seq_profile_name);
      start_parallel_profile_ptask(d, seq_profile_name, hostfile, progress, double(a.Size()));
    }
  }
}

unsigned int get_number_of_rank_from_hostfile(std::string hostfile) {
  unsigned int number_of_lines = 0;
  FILE *infile = fopen(hostfile.c_str(), "r");
  int ch;

  while (EOF != (ch=getc(infile)))
      if ('\n' == ch)
          ++number_of_lines;

  printf("number of lines: %u\n", number_of_lines);
  return number_of_lines;
}

void start_smpi(std::string binary, std::string deployment, unsigned int number_of_rank) {
  auto engine = simgrid::s4u::Engine::get_instance();
  SMPI_init();

  printf("%s\n", binary.c_str());
      // Ouverture de la bibliothèque
  void *hndl = dlopen(binary.c_str(), RTLD_LAZY);
  if(hndl == NULL)
  {
      printf("erreur dlopen : %s\n", dlerror());
      exit(EXIT_FAILURE);
  }

  xbt_main_func_t smpi_main = (xbt_main_func_t) dlsym(hndl, "main");
  if (smpi_main == NULL)
  {
      printf("erreur dlsym : %s\n", dlerror());
      dlclose(hndl);
      exit(EXIT_FAILURE);
  }

  SMPI_app_instance_register("smpi_main", smpi_main, number_of_rank);

  printf("deployment: %s\n", deployment.c_str());
  engine->load_deployment(deployment.c_str());

}

void notify_smpi_end() {
  simgrid::s4u::Mailbox* smpi_end  = simgrid::s4u::Mailbox::by_name("smpi_end");
  smpi_end->put((void*)"akadoc", 0);
}

void start_simulation(map<string, docopt::value> args) {
  auto master_host = args["<master_host>"].asString();
  auto is_tcpkali_ptask = args["--ptask"].asBool();

  std::vector<simgrid::s4u::ActorPtr> actors_for_interference;

  if(args["interference"].asBool() == true) {
    auto idle_time     = args["--idle"].asLong();
    auto interf_time   = args["--interf"].asLong();
    std::string server = args["--server"].asString();
    std::string client = args["--client"].asString();

    if(is_tcpkali_ptask) {
      auto tcpkali_ptask_actor = simgrid::s4u::Actor::create("tcpkali_ptask",
          simgrid::s4u::Host::by_name(client), // host
          tcpkali_ptask, // function
          client, server, idle_time, interf_time); // parameters
      actors_for_interference.push_back(tcpkali_ptask_actor);
    } else {
      auto ser = simgrid::s4u::Actor::create("tcpkali_server",
          simgrid::s4u::Host::by_name(server),
          tcpkali_server);

      auto cli = simgrid::s4u::Actor::create("tcpkali_client",
          simgrid::s4u::Host::by_name(client), // host
          tcpkali_client, // function
          ser, idle_time, interf_time); // parameters

      actors_for_interference.push_back(ser);
      actors_for_interference.push_back(cli);
    }
  }

  if(args["nb_connections"].asBool()) {
    std::string server = args["--server"].asString();
    std::string client = args["--client"].asString();

    std::string mailbox_name_rec  = "mb_tcpkali_1";
    std::string mailbox_name_send = "mb_tcpkali_2";
    if(is_tcpkali_ptask) {
      auto tcpkali_ptask_actor = simgrid::s4u::Actor::create("tcpkali_ptask",
          simgrid::s4u::Host::by_name(client), // host
          tcpkali_ptask_nb_connections, // function
          client, server, args["<nb_connections>"].asLong()); // parameters
      actors_for_interference.push_back(tcpkali_ptask_actor);
    } else {
      auto tcpkali_actor_cli = simgrid::s4u::Actor::create("tcpkali_ptask_1",
          simgrid::s4u::Host::by_name(client), // host
          tcpkali_nb_connections, // function
          mailbox_name_send, mailbox_name_rec, args["<nb_connections>"].asLong());

      auto tcpkali_actor_ser = simgrid::s4u::Actor::create("tcpkali_ptask_2",
          simgrid::s4u::Host::by_name(server), // host
          tcpkali_nb_connections, // function
          mailbox_name_rec, mailbox_name_send, args["<nb_connections>"].asLong());

      actors_for_interference.push_back(tcpkali_actor_cli);
      actors_for_interference.push_back(tcpkali_actor_ser);
    }
  }

  if(args["smpi"].asBool()) {
    xbt_assert(false, "Not supported");
    auto hostfile = args["--hostfile"].asString();
    auto deployment = args["<deployment>"].asString();
    auto binary = args["<binary>"].asString();

    auto number_of_rank = get_number_of_rank_from_hostfile(hostfile);
    start_smpi(binary, deployment, number_of_rank);

    unsigned int number_terminated = 0;

    // simgrid::s4u::Actor::on_termination.connect(
    //   [&](simgrid::s4u::Actor const& actor) {
    //       if (actor.get_name() == "smpi_main") {
    //         number_terminated++;
    //         // Print when the last mpi rank has terminated
    //         if(number_terminated == number_of_rank) {
    //           XBT_INFO("SMPI terminates now.");
    //           simgrid::s4u::Actor::create("smpi_ending", simgrid::s4u::Host::by_name(master_host), notify_smpi_end);
    //         }
    //       }
    //     });
    //
    simgrid::s4u::Mailbox* smpi_end  = simgrid::s4u::Mailbox::by_name("smpi_end");
    auto data = smpi_end->get<char>();
    XBT_INFO("SPMI Finished %s", data);

  } else {
    auto hostfile = args["--hostfile"].asString();
    auto profile_file = args["--profile"].asString();
    auto ptask = simgrid::s4u::Actor::create("ptask",
        simgrid::s4u::Host::by_name(master_host),
        start_parallel_task, profile_file, hostfile);

    ptask->join();
    XBT_INFO("Parallel task finished");
  }

  if(args["interference"].asBool() == true || args["nb_connections"].asBool()) {
    for(auto actor: actors_for_interference) {
      actor->kill();
    }
  }
}

static const char USAGE[] =
R"(Simulation parameters.

    Usage:
      main <platform_file> <master_host> -p <profile> -H hostfile [--sg-cfg <opt_name:opt_value>...]
      main interference <platform_file> <master_host>
            -p <profile>
            -d <idle_time>
            -t <interf_time>
            -s <server>
            -c <client>
            -H hostfile
            [--platform-model <model>]
            [--ptask]
            [--sg-cfg <opt_name:opt_value>...]
      main nb_connections <platform_file> <master_host>
            -p <profile>
            -s <server>
            -c <client>
            -H hostfile
            -n <nb_connections>
            [--platform-model <model>]
            (--ptask | --com-via-box)
            [--sg-cfg <opt_name:opt_value>...]
      main nb_connections smpi
            <platform_file>
            <master_host>
            <deployment>
            <binary>
            -s <server>
            -c <client>
            -H hostfile
            -n <nb_connections>
            (--ptask | --com-via-box)
            [--platform-model <model>]
            [--sg-cfg <opt_name:opt_value>...]

    Options:
      -h --help                           Show this screen.
      --version                           Show version.
      -p, --profile <profile>             The json profile containing the parallel task inputs.
      -d, --idle <idle_time>              Idle time in the interference period.
      -t, --interf <interf_time>          Interference time in the interference period.
      -H, --hostfile <hostfile>           Hostfile mapping.
      -s, --server <server>               Host for tcpkali server.
      -P, --ptask                         Simulate tcpkali using ptask.
      -c, --client <client>               Host for tcpkali client.
      --sg-cfg <opt_name:opt_value>       Forwards a given option_name:option_value to SimGrid.
                                          Refer to SimGrid configuring documentation for more information.
      --smpi-args <args>                  Parameters to forward to smpi when the subcommand is used.
      --platform-model <model>            Name of the SimGrid platform model to use [default: Ptask_L07].
)";

int main(int argc, char* argv[])
{

    map<string, docopt::value> args = docopt::docopt(USAGE,
                                                    { argv + 1, argv + argc },
                                                     true,
                                                     "0.0.a");

    auto platform_file = args["<platform_file>"].asString();
    auto simgrid_config = args["--sg-cfg"].asStringList();
    // Instantiate SimGrid
    s4u::Engine engine(&argc, argv);

    // xbt_log_control_set("ker_bmf.thres:debug");

    auto model = args["--platform-model"].asString();
    if(model == "Ptask_L07") {
        engine.set_config("host/model:ptask_L07");
        printf("Changing model to Ptask_L07 %s\n", model.c_str());
    } else if(model == "BMF") {
        engine.set_config("host/model:ptask_BMF");
        printf("Changing model to BMF %s\n", model.c_str());
    }

    engine.load_platform(platform_file.c_str());
    // Setting SimGrid configuration options, if any
    for (const string & cfg_string : simgrid_config)
    {
      engine.set_config(cfg_string);
    }


    auto master_host = args["<master_host>"].asString();

    simgrid::s4u::Actor::create("simulation",
        simgrid::s4u::Host::by_name(master_host), // host
        start_simulation, // function
        args); // parameters

    engine.run();

    XBT_INFO("Simulation done.");

    if(args["smpi"].asBool())
      SMPI_finalize();


    return 0;
}
