#! /usr/bin/env nix-shell
#! nix-shell ../shell.nix -i python3

import sys
import xml.etree.ElementTree as ET

if len(sys.argv) < 3:
    print(
        "USAGE\n generate_deployment.py hostfile deploymentfile [ smpi_args1 smpi_arg2 ... ]"
    )
    exit(1)

hostfile = sys.argv[1]
output_deployment = sys.argv[2]

index_smpi = 3
smpi_args = []
if len(sys.argv) > index_smpi:
    smpi_args = sys.argv[index_smpi:]

base = """<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">
<platform version="4.1">
</platform>
"""

platform = ET.fromstring(base)

lines = []
with open(hostfile) as f:
    lines = [line.rstrip() for line in f]

actors = []
for i in range(len(lines)):
    actor = ET.Element("actor", {"host": lines[i], "function": "smpi_main"})
    for smpi_arg in smpi_args:
        ET.SubElement(actor, "argument", {"value": smpi_arg})

    # SMPI Informations
    ET.SubElement(actor, "prop", {"id": "instance_id", "value": "smpi_main"})
    ET.SubElement(actor, "prop", {"id": "rank", "value": str(i)})

    platform.append(actor)

tree = ET.ElementTree(platform)
with open(output_deployment, "wb") as f:
    f.write(
        '<?xml version="1.0"?>\n<!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">\n'.encode(
            "utf8"
        )
    )
    tree.write(f, "utf-8")
