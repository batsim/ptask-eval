"""
Python script to generate a ptask.
It run the application gemsmpi in smpi, using a rankfile and generates a ptask.

Problem ... I only have the data of the data needed to generate the ptask.
"""
import exptools
import os
import json
import yaml

result_dir = "data"
nb_divisons = 50
nbrank = 256
matrice_len = 80000
nb_loop = 200


# This file is an input
rankfile = "../g5k/data/graphkali/grisou/05-26-21:23:50:07/instances/gemmpi.alone/5f444364/rankfile"

# This file is an output -> This file
hostfile = "hostfiles/graphkali_hostfile"

# If the file `hostfile` already exists the ptask can be
# generated without the rankfile (i.e comment this line)
os.system(
    f'cat "{rankfile}" | sed -E "s/rank [0-9]+=(.*) slot=[0-9]+/\\1/" > "{hostfile}"'
)

smpi_binary = (
    "$(which gemsmpi) -s {nb_subdivisions} {matrice_dims} -l {nb_loop}".format(
        nb_subdivisions=nb_divisons, matrice_dims=matrice_len, nb_loop=nb_loop
    )
)

# Running the simulations
metadata = exptools.play_gemmpi_smpi(
    expdir=result_dir,
    matrice_dims=matrice_len,
    nbrank=nbrank,
    nb_subdivisions=nb_divisons,
    platform_file="platforms/paravance.xml",
    hostfile=hostfile,
    extra_smpi_params="--cfg=smpi/tmpdir:/tmp",  # "--cfg=smpi/bcast:ompi",
    # If simulation already exists you can set no_exec to True
    no_exec=True,
    trace_tit=False,
    nb_max_loop=nb_loop,
    smpi_binary=smpi_binary,
)

with open(
    "profiles/paje_nbhost-32_nbrank-256_dims-80000_subdivisions-50/metadata.yaml", "r"
) as stream:
    try:
        metadata = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)


# Generating the ptask profile
tracefile = metadata["outputs"]["files"]["paje_trace"]
inst_result_dir = metadata["outputs"]["result_dir"]

profile, hostfile = exptools.create_profile_comm_and_task_aggregated(metadata, name="profile")
with open(os.path.join(inst_result_dir, "profile-aggregated.json"), "w") as tfile:
    print(
        json.dumps(profile, sort_keys=True, indent=2, separators=(",", ": ")),
        file=tfile,
    )

with open(os.path.join(inst_result_dir, "hostfile-aggregated"), "w") as tfile:
    print(hostfile, file=tfile)

profile, mapping = exptools.create_profile_with_mapping_from_smpi(
    metadata, name="profile"
)

with open(os.path.join(inst_result_dir, "profile.json"), "w") as tfile:

    print(
        json.dumps(profile, sort_keys=True, indent=2, separators=(",", ": ")),
        file=tfile,
    )

root, profile_list, mapping = exptools.create_profile_sequence_from_smpi(
    metadata, name="profile", nb_loops_to_aggregate=50
)
profiles = [root] + profile_list
profile_dict = dict()

for pro in profiles:
    for key in pro:
        profile_dict[key] = pro[key]

with open(os.path.join(inst_result_dir, "aggregated_50.json"), "w") as tfile:
    print(
        json.dumps(profile_dict, sort_keys=True, indent=2, separators=(",", ": ")),
        file=tfile,
    )


root, profile_list, mapping = exptools.create_profile_sequence_from_smpi(
    metadata, name="profile", nb_loops_to_aggregate=16
)
profiles = [root] + profile_list
profile_dict = dict()
for pro in profiles:
    for key in pro:
        profile_dict[key] = pro[key]

with open(os.path.join(inst_result_dir, "aggregated_16.json"), "w") as tfile:
    print(
        json.dumps(profile_dict, sort_keys=True, indent=2, separators=(",", ": ")),
        file=tfile,
    )
