#! /usr/bin/env nix-shell
#! nix-shell ../shell.nix -i python3
"""
Script to run a ptask variating the number of connections (comm) in SG.
"""
import os
import subprocess
import re
import pandas as pd
import shutil

data_dir = "profiles/paje_nbhost-32_nbrank-256_dims-80000_subdivisions-50/"
hostfile = os.path.join(data_dir, "hostfile")
platform = os.path.join(data_dir, "platform.xml")
profile = os.path.join(data_dir, "profile.json")
server_interference = "10.144.0.16"
client_interference = "10.144.4.16"
master_host = "10.144.0.1"
nb_connections = 0

# # Command for ptask
# base_cmd = [
#     "./build/sg_simulation",
#     "nb_connections",
#     platform,
#     master_host,
#     "-p",
#     profile,
#     "-s",
#     server_interference,
#     "-c",
#     client_interference,
#     "-H",
#     hostfile,
# ]
# 
# number_of_tests = 33
# 
# data = []
# for n in range(number_of_tests):
#     cmd = base_cmd + ["-n", str(n), "--com-via-box"]
#     p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#     out = p.stderr.decode("utf-8")
#     print(out)
#     found = re.search(
#         "\[10.144.0.1:simulation:\(1\) (.+?)\] \[mwe/INFO\] Parallel task finished", out
#     ).group(1)
#     print("Ptask took: {} s".format(float(found)))
#     data.append((n, float(found), "ptask", "Ptask_L07"))

# Command for aggregated ptask
hostfile = os.path.join(data_dir, "hostfile-aggregated")
profile = os.path.join(data_dir, "profile-aggregated.json")
platform = os.path.join(data_dir, "platform-no-core.xml")

base_cmd = [
    "./build/sg_simulation",
    "--platform-model",
    "BMF",
    "nb_connections",
    platform,
    master_host,
    "-p",
    profile,
    "-s",
    server_interference,
    "-c",
    client_interference,
    "-H",
    hostfile,
]

number_of_tests = 33

data = []
for n in range(number_of_tests):
    cmd = base_cmd + ["-n", str(n), "--com-via-box"]
    print(" ".join(cmd))
    p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = p.stderr.decode("utf-8")
    print(out)
    found = re.search(
        "\[10.144.0.1:simulation:\(1\) (.+?)\] \[mwe/INFO\] Parallel task finished", out
    ).group(1)
    print("Ptask took: {} s".format(float(found)))
    data.append((n, float(found), "ptask-BMF", "Ptask-BMF"))

# Command for SMPI

# deployment = os.path.join(data_dir, "deployment.xml")
# hostfile = os.path.join(data_dir, "hostfile")
# platform = os.path.join(data_dir, "platform.xml")
# 
# binary = shutil.which("gemsmpi")
# base_cmd = [
#     "./build/sg_simulation",
#     "nb_connections",
#     "smpi",
#     platform,
#     master_host,
#     deployment,
#     binary,
#     "-s",
#     server_interference,
#     "-c",
#     client_interference,
#     "-H",
#     hostfile,
# ]
# 
# for n in range(number_of_tests):
#     cmd = base_cmd + ["-n", str(n), "--com-via-box"]
#     for model in ["Ptask_L07", "SMPI default"]:
#         model_parameter = (
#             ["--platform-model", model]
#             if model == "Ptask_L07"
#             else ["--platform-model", "Default"]
#         )
#         cmd_with_model = cmd + model_parameter
# 
#         p = subprocess.run(
#             cmd_with_model, stdout=subprocess.PIPE, stderr=subprocess.PIPE
#         )
#         out = p.stderr.decode("utf-8")
#         print(out)
#         found = re.search("\[(.+?)\] \[mwe/INFO\] SMPI terminates now.", out).group(1)
#         print("SMPI took: {} s".format(float(found)))
#         data.append((n, float(found), "smpi", model))
# 
df = pd.DataFrame(
    data,
    columns=["number_of_connections", "runtime_predicted", "application_type", "model"],
)
df.to_csv("number_of_connections.BMF.csv", index=False)
