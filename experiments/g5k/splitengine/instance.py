import copy
import datetime
import logging
import os
import subprocess
import time
import uuid
from subprocess import PIPE

import yaml

from .remote_process import RemoteProcess


class InstanceError(Exception):
    pass


def monitor_node_process(
    node,
    key="experiment_keys/id_rsa",
    gateway=None,
    freq=1,
    netface=None,
    performance_counters=["cpu_cycles", "instructions"],
    outputfile="./mojitos.csv",
):

    # Base command
    command_params = [
        "mojitos",
        "-p",
        ",".join(performance_counters),
        "-f",
        str(freq),
        "-o",
        outputfile,
    ]

    # Add network monitoring if activated
    if netface is not None:
        command_params += ["-d", netface]
    mojitos_command = " ".join(command_params)

    if gateway is not None:
        return RemoteProcess(node, mojitos_command, gateway=gateway, access_key=key)
    else:
        return RemoteProcess(node, mojitos_command, access_key=key)


class Instance:
    """"""

    def __init__(
        self,
        configuration_file,
        netgroup_1,
        netgroup_2,
        root_folder="/home/afaure/batmet_experiments",
        key="experiment_keys/id_rsa",
    ):
        # Inputs
        with open(configuration_file) as f:
            # use safe_load instead load
            self.in_data = yaml.safe_load(f)
        print(self.in_data)

        # self.nb_host_per_group = int(self.in_data["hosts_per_netgroup"])
        self.key = key
        self.root_folder = root_folder
        self.group_1 = netgroup_1
        self.group_2 = netgroup_2

        # Get hosts information
        self.g1_hosts = copy.deepcopy(netgroup_1.get_g5k_host())
        self.g1_ips = netgroup_1.get_corresponding_ips(self.g1_hosts)
        # Init with all node, but it shall be reset at allocation time
        self.g1_used_ips = []

        self.g2_hosts = copy.deepcopy(netgroup_2.get_g5k_host())
        self.g2_ips = netgroup_2.get_corresponding_ips(self.g2_hosts)
        self.g2_used_ips = []

        self.router = self.group_1.get_router_g5k_hostname()

        # Instance
        # Generate an unique name for this run
        self.run_name = str(uuid.uuid4())[:8]
        self.is_finished = False

        # Folder in which files will be generated
        self.remote_folder_name = os.path.join(
            "/home/g5k", self.in_data["instance_name"], self.run_name
        )

        self.local_folder_name = os.path.join(
            root_folder, self.in_data["instance_name"], self.run_name
        )

        # Copy resources for allocations
        self.g1_available_hosts = copy.deepcopy(netgroup_1.get_g5k_host())
        self.g2_available_hosts = copy.deepcopy(netgroup_2.get_g5k_host())
        # Contains applications that needs to execute till the end (or the end of the timeout).
        self.applications = []
        # Contains the applications that can be stopped when all applications are done.
        self.besteffort_applications = []

        self.out_data = {
            "network": {"instance_hosts": {}},
            "tcpkali": {},
            "mpi": {},
        }

    def init_applications(self):
        """Creates an allocation for every applications. And detect best-effort applications."""
        applications = self.in_data["applications"]
        self.g1_used_ips = []
        self.g2_used_ips = []

        for application in applications:
            logging.debug("Configure: {}", application)
            # First try to create an allocation for the application
            # TODO: Handle expection
            allocated_resources = self.allocate_resources(application)
            self.g1_used_ips = self.g1_used_ips + self.group_1.get_corresponding_ips(
                allocated_resources[0]
            )
            self.g2_used_ips = self.g2_used_ips + self.group_2.get_corresponding_ips(
                allocated_resources[1]
            )

            logging.info(
                "Allocated resources for: {}\ng1: {}\ng2: {}".format(
                    application["type"], allocated_resources[0], allocated_resources[1]
                )
            )
            if application["type"] == "mpi":
                # .applications needs to import experiment which leads to circular dependency.
                # However, in applications/ the import on experiment is only for the type of Instance (mypy)
                from .applications.mpi import Mpi

                app_instance = Mpi(
                    copy.deepcopy(application),
                    self,
                    allocated_resources[0],
                    allocated_resources[1],
                )
            elif application["type"] == "tcpkali":
                from .applications.tcpkali import TcpKali

                app_instance = TcpKali(
                    copy.deepcopy(application),
                    self,
                    allocated_resources[0],
                    allocated_resources[1],
                )

            elif application["type"] == "graphkali":
                from .applications.graphkali import GraphKali

                app_instance = GraphKali(
                    copy.deepcopy(application),
                    self,
                    allocated_resources[0],
                    allocated_resources[1],
                )

            logging.info("Application generated")
            if application["best_effort"]:
                self.besteffort_applications.append(app_instance)
            else:
                self.applications.append(app_instance)
            logging.info("Application registred")

    def allocate_resources(self, application):
        """Allocates resources for a given application.
        The ressources are removed from the variables self.g{1,2}_available_nodes.
        """
        allocation = copy.deepcopy(application["allocation"])
        g1_nodes = []
        g2_nodes = []
        if "g1" in allocation["hosts_per_group"]:
            nb_nodes_needed = int(allocation["hosts_per_group"]["g1"])
            # Check that we have enough free resources
            if len(self.g1_available_hosts) >= nb_nodes_needed:
                g1_nodes = self.g1_available_hosts[:nb_nodes_needed]
                self.g1_available_hosts = self.g1_available_hosts[nb_nodes_needed:]
            else:
                logging.error(
                    "Not enough resources in {}. Resources available: {}, needed: {}".format(
                        "g1", len(self.g1_available_hosts), nb_nodes_needed
                    )
                )
                raise InstanceError("Not enough resources available")

        if "g2" in allocation["hosts_per_group"]:
            nb_nodes_needed = int(allocation["hosts_per_group"]["g2"])
            # Check that we have enough free resources
            if len(self.g2_available_hosts) >= nb_nodes_needed:
                g2_nodes = self.g2_available_hosts[:nb_nodes_needed]
                self.g2_available_hosts = self.g2_available_hosts[nb_nodes_needed:]
            else:
                logging.error(
                    "Not enough resources in {}. Resources available: {}, needed: {}".format(
                        "g2", len(self.g2_available_hosts), nb_nodes_needed
                    )
                )
                raise InstanceError("Not enough resources available")

        return (g1_nodes, g2_nodes)

    def clean_host_before_start(self):
        logging.info("clean mojitos on all hosts")
        self.execute_on_machines(
            self.g1_used_ips + self.g2_used_ips,
            "pkill mojitos",
            wait=False,
            use_router=True,
        )
        self.execute_on_router("pkill mojitos")

        logging.info("Clean applications")
        for app in self.applications + self.besteffort_applications:
            logging.info("clean: {}".format(app))
            app.clean()
            logging.info("clean finished: {}".format(app))

    def save(self):
        logging.info("saving instance state")
        netg1_state = self.group_1.get_state()
        netg2_state = self.group_2.get_state()

        self.out_data["instance_name"] = self.in_data["instance_name"]
        self.out_data["run_id"] = self.run_name
        self.out_data["nodes"] = {
            "g1_allocated": self.g1_used_ips,
            "g2_allocated": self.g2_used_ips,
        }

        self.out_data["network"]["net_g1"] = netg1_state
        self.out_data["network"]["net_g2"] = netg2_state
        self.out_data["network"]["instance_hosts"] = dict()
        self.out_data["network"]["instance_hosts"]["g1"] = self.g1_hosts
        self.out_data["network"]["instance_hosts"]["g2"] = self.g2_hosts

        self.out_data["applications"] = []
        for app in self.applications:
            self.out_data["applications"].append(app.info())

        self.out_data["be_applications"] = []
        for app in self.besteffort_applications:
            self.out_data["be_applications"].append(app.info())

        self.output_sate_file = os.path.join(self.local_folder_name, "out.yaml")

        with open(self.output_sate_file, "w") as outfile:
            yaml.safe_dump(self.out_data, outfile, default_flow_style=False)

        self.output_sate_file = os.path.join(self.local_folder_name, "in.yaml")
        with open(self.output_sate_file, "w") as outfile:
            yaml.safe_dump(self.in_data, outfile, default_flow_style=False)

    def latencies_with_router(self):
        logging.info("measure nodes latency")
        supected_nodes = []

        for machine in self.g1_available_hosts + self.g2_available_hosts:
            if machine in self.g1_available_hosts:
                ip = self.group_1.get_ip_for_host(machine)
            else:
                ip = self.group_2.get_ip_for_host(machine)

            logging.info("testing host {} - {}".format(machine, ip))
            start = datetime.datetime.now()
            r = RemoteProcess(
                ip,
                "hostname",
                gateway=self.router,
                extra_ssh_opts=["-f"],
                access_key=self.key,
            )
            r.run(timeout=5)
            end = datetime.datetime.now()

            if r.timeout_reached:
                logging.warning("problem with node {} - {} ".format(machine, ip))
                supected_nodes.append(machine)
            else:
                logging.info("host {} - {} has {} latency".format(machine, ip, end - start))

        logging.warning("nodes will be removed: {}".format(supected_nodes))

        self.g1_available_hosts = [x for x in self.g1_available_hosts if x not in supected_nodes]
        self.g2_available_hosts = [x for x in self.g2_available_hosts if x not in supected_nodes]

    def execute_on_machines(
        self,
        machines,
        command,
        use_router=False,
        wait=True,
        key="experiment_keys/id_rsa",
    ):
        logging.info(
            "execute {} on {} hosts (waiting ? {})".format(command, len(machines), wait)
        )
        processes = []
        if use_router is True:
            for machine in machines:
                # This one is extremely slow, so I basicaly disable it by setting and False
                if wait and False:
                    r = RemoteProcess(
                        machine, command, gateway=self.router, access_key=self.key
                    )
                    r.run_bg()
                    processes.append(r)
                else:
                    r = RemoteProcess(
                        machine,
                        command,
                        gateway=self.router,
                        extra_ssh_opts=["-f"],
                        access_key=self.key,
                    )
                    r.run_bg()
                    processes.append(r)

            logging.info(
                "{} processe have been launched - waiting.".format(len(processes))
            )
            # if wait is false, it should be empty
            for p in processes:
                logging.info("wait for {} host {}".format(p, p.hostname))
                p.wait()

        else:
            for machine in machines:
                r = RemoteProcess(machine, command, access_key=self.key)
                if wait and False:
                    r.run()
                else:
                    r.run_bg()

    def execute_on_router(self, command, wait=True):
        r = RemoteProcess(self.router, command, access_key=self.key)
        if wait:
            r.run()
        else:
            r.run_bg()

    def clean_monitoring(self):
        # Create a folder on for the exp on each hosts
        # logging.info("Cleaning mojitos processes")
        # for task in self.monitoring_processes:
        #    task.terminate()

        logging.info("clean mojitos on all hosts")
        self.execute_on_machines(
            self.g1_used_ips + self.g2_used_ips,
            "pkill mojitos",
            wait=False,
            use_router=True,
        )
        self.execute_on_router("pkill mojitos")

    def gather_files(self):
        # We use the "instance folder" name which should be the parent of all
        # run from this instance
        os.path.join(self.root_folder, self.in_data["instance_name"])

        tar_tmpl = "tar czvf {archive}.tar.gz -C {folder} ."
        self.execute_on_machines(
            self.g1_used_ips + self.g2_used_ips,
            tar_tmpl.format(
                archive=self.run_name + ".$(hostname)", folder=self.remote_folder_name
            ),
            use_router=True,
        )

        self.execute_on_router(
            tar_tmpl.format(
                archive=self.run_name + ".$(hostname)",
                folder=self.remote_folder_name,
                remote_dest=self.remote_folder_name,
            )
        )

        for host in self.g1_used_ips + self.g2_used_ips:
            addr = "g5k@%s" % host
            path = ":" + self.run_name + ".*.tar.gz"
            proxy_jump = [
                "-o",
                'ProxyCommand="ssh -i {} -W %h:%p g5k@{}"'.format(
                    self.key, self.router
                ),
            ]
            exec_array = (
                ["scp", "-i", self.key]
                + proxy_jump
                + [addr + path, self.local_folder_name]
            )

            subprocess.run(" ".join(exec_array), shell=True, stderr=PIPE, stdout=PIPE)

        router_addr = "g5k@%s" % self.router
        router_path = ":" + self.run_name + "." + self.router + ".tar.gz"
        subprocess.run(
            ["scp", "-i", self.key, router_addr + router_path, self.local_folder_name]
        )

    def get_timestamp_monotonic_matching(self):
        command = 'python3 -c "import time;print(\\"{};{}\\".format(time.time(),time.monotonic()))"'
        self.out_data["time_matching"] = dict()
        for host in self.g1_used_ips + self.g2_used_ips + [self.router]:

            res = (
                RemoteProcess(
                    host,
                    command,
                    access_key=self.key,
                    remote_stdout="/tmp/myclock",
                    remote_stderr="/tmp/myclock.err",
                    gateway=self.router,
                )
                .run()
                .get_stdout()
            )
            print(res)
            times = res.split(";")
            print(times)
            self.out_data["time_matching"][host] = {
                "timestamp": float(times[0]),
                "monotonic": float(times[1]),
            }

    def monitor_hosts(self):
        # Configure the monitoring
        logging.info("create processes for monitoring")
        netfaces = self.in_data["mojitos"]["network"]
        self.monitoring_processes = []

        for netface in netfaces:
            for host in self.g1_used_ips:
                # Mojitos output
                g5kname = self.group_1.get_host_for_ip(host)
                mojitos_out = os.path.join(
                    self.remote_folder_name, "%s.%s.mojitos.csv" % (g5kname, netface)
                )
                self.monitoring_processes.append(
                    monitor_node_process(
                        host,
                        outputfile=mojitos_out,
                        gateway=self.router,
                        netface=netface,
                        key=self.key,
                    )
                )

            for host in self.g2_used_ips:
                # Mojitos output
                g5kname = self.group_2.get_host_for_ip(host)
                mojitos_out = os.path.join(
                    self.remote_folder_name, "%s.%s.mojitos.csv" % (g5kname, netface)
                )
                self.monitoring_processes.append(
                    monitor_node_process(
                        host,
                        outputfile=mojitos_out,
                        gateway=self.router,
                        netface=netface,
                        key=self.key,
                    )
                )

            logging.info(
                "create processes for monitoring the router:_{}".format(netface)
            )
            mojitos_out = os.path.join(
                self.remote_folder_name, "%s.%s.mojitos.csv" % (self.router, netface)
            )
            self.monitoring_processes.append(
                monitor_node_process(
                    self.router,
                    outputfile=mojitos_out,
                    gateway=self.router,
                    netface=netface,
                    key=self.key,
                )
            )

        logging.info("Start monitoring processes")
        for task in self.monitoring_processes:
            task.run_bg()

    def run_instance(self):
        logging.info("Prepare local files for instance: %s", self.local_folder_name)

        logging.info("looking for faulty nodes")
        self.latencies_with_router()

        logging.info("Creating applications and making allocations.")
        self.init_applications()

        logging.info("cleaning host")
        self.clean_host_before_start()

        # This function is buggy, and not crucial
        # self.get_timestamp_monotonic_matching()
        os.makedirs(self.local_folder_name)

        # Create a folder on for the exp on each hosts
        logging.info("create folder for exp: {}".format(self.remote_folder_name))
        self.execute_on_machines(
            self.g1_ips + self.g2_used_ips,
            "mkdir -p %s" % self.remote_folder_name,
            use_router=True,
        )

        self.execute_on_router("mkdir -p %s" % self.remote_folder_name)

        logging.info("Prepare applications")
        for application in self.applications + self.besteffort_applications:
            application.prepare()

        logging.info("Start monitoring")
        self.monitor_hosts()
        time.sleep(5)

        # Save current state for tracing
        self.save()

        # Get a time so every application can start at the same time
        starts_at = int(time.time() + 10)
        logging.info("run applications")
        for application in self.applications + self.besteffort_applications:
            application.run(starts_at=starts_at)

        timeout = int(self.in_data["timeout"])
        logging.info("Instance Timeout %s" % self.in_data["timeout"])
        for application in self.applications:
            application.wait(timeout)

        logging.info("Clean best effort")
        for be_application in self.besteffort_applications:
            be_application.clean()

        # for application in self.besteffort_applications + self.applications:
        #    application.clean()

        time.sleep(int(2))

        logging.info("Clean monitoring")
        self.clean_monitoring()

        logging.info("Gather data")
        self.gather_files()
        self.save()

        #  self.clean_host_before_start()
