import ipaddress
import logging
import os
import subprocess

from .networkgroup import NetworkGroup


# Installing dependencies on hosts
# Including nix, mpi, tcpkali, git
# In addition one can connect without password using
# `ssh -i experiment_keys/id_rsa g5k@{node}` to any node of the cluster
def setup_g5k_hosts(hostnames, job_id, ssh_keys):
    processes = []
    for node in hostnames:
        pass
        logging.info("Installing host={}".format(node))

        # This folder is used both to find the setup host file and the script that will
        # be deploied on the host.
        dir_path = os.path.dirname(os.path.realpath(__file__))
        scripts_to_deploy = os.path.join(dir_path, "scripts")
        logging.info("Getting scripts from path: {}".format(dir_path))

        install = (subprocess.Popen(
            [
                os.path.join(dir_path, "./scripts/setup_host.sh"),
                node,
                ssh_keys,
                scripts_to_deploy,
            ],
            stdout=open(job_id + "_" + node + ".log_install.out", "wb"),
            stderr=open(job_id + "_" + node + ".log_install.err", "wb"),
        ), node)

        processes.append(install)

    for inst in processes:
        logging.info("wait install on node: {}.".format(inst[1]))
        inst[0].communicate()


def configure_network(hostnames, nodes_list):
    logging.info("Network configuration")
    # Picking one node to do the router
    router_node = "{}".format(nodes_list[0])
    logging.info("Router node={}".format(router_node))

    # Gather reserved subnets
    # so instead of using it, we parse the output of -n option.
    subnets_raw = subprocess.getoutput("g5k-subnets -p").split("\n")
    subnets = list(map(lambda subnet: ipaddress.ip_network(subnet), subnets_raw))

    logging.info("subnets: {}".format(subnets))
    # Creating two groups of nodes
    nodes_jobs = nodes_list[1:]
    if len(nodes_jobs) % 2 != 0:
        logging.warning("Odd number of node: {}".format(str(len(nodes_jobs))))

    g1_nodes = nodes_jobs[0 : int(len(nodes_jobs) / 2)]
    g2_nodes = nodes_jobs[int(len(nodes_jobs) / 2) :]

    network_g1 = NetworkGroup("G1", subnets[0], g1_nodes, router_node, "eno1")
    network_g1.log()
    network_g1.configure_nodes_ip()
    network_g1.configure_router_ip()

    network_g2 = NetworkGroup("G2", subnets[1], g2_nodes, router_node, "eno2")
    network_g2.log()
    network_g2.configure_nodes_ip()
    network_g2.configure_router_ip()

    # add route from g1 to g2 and vice-versa
    network_g1.configure_routes(network_g2)
    network_g2.configure_routes(network_g1)

    # Return the networks so we can generate hostname
    return [network_g1, network_g2]
