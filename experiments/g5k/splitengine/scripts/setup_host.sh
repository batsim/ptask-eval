#!/usr/bin/env bash
# This script is part of the experiment for interferences.

# $1: address of a host to access with SSH
# $2: path of the folder containing the ssh keys to deploy on the machines.

set -eux

# Address of the host to configure
HOST=$1
# Path to the folder containing the experiment ssh key
KEYS=$2
# Path containing files such as nix expression and bash script necessary for the experiment
DEPLOYMENT_FILES=$3

# The name of the user for the experiment
# FIXME: This variable cannot be used in this as we need to wrap some command with ', which prevents variable for being intepreted.
# USERNAME="g5k"

SCRIPTS_FOLDER="/home/g5k/.scripts"

ssh -o "StrictHostKeyChecking=no" root@${HOST} 'hostname'

# change root bash
ssh root@${HOST} 'usermod --shell /usr/bin/bash root'

ssh root@${HOST} 'apt update && apt install -y --force-yes sshpass'

if ssh root@${HOST} 'id g5k' >/dev/null 2>&1
then
  echo "g5k" user already exist
else
  #  create_user:
  ssh root@${HOST} 'useradd --create-home --password $(openssl passwd -crypt g5k) -G sudo g5k -s /usr/bin/bash'
fi

# allow sudo without password
ssh root@${HOST} 'echo "g5k ALL=(ALL) NOPASSWD: ALL" | sudo EDITOR="tee -a" visudo'

# install_nix as g5k user
ssh root@${HOST} 'echo 1 > /proc/sys/kernel/unprivileged_userns_clone'

# enable perf_event polling for mojitos
ssh root@${HOST} 'echo "-1" > /proc/sys/kernel/perf_event_paranoid'

# Configure the key on the hosts
ssh root@${HOST} mkdir -p /home/g5k/.ssh
scp -r ${KEYS}/* root@${HOST}:/home/g5k/.ssh

ssh root@${HOST} 'chown -R g5k:g5k /home/g5k/.ssh'
ssh root@${HOST} 'chmod 600 /home/g5k/.ssh/id_rsa.pub'
ssh root@${HOST} 'chmod 400 /home/g5k/.ssh/id_rsa'

# remove old script if already exists
ssh root@${HOST} "rm -rf ${SCRIPTS_FOLDER}"

# Deploy the files and script needed for the execution
ssh root@${HOST} "mkdir -p  ${SCRIPTS_FOLDER}"
scp -r ${DEPLOYMENT_FILES}/* root@${HOST}:${SCRIPTS_FOLDER}

ssh root@${HOST} 'chown -R g5k:g5k /home/g5k/'

# Installing nix and dependencies
ssh g5k@${HOST} -i ${2}/id_rsa << EOF
# Need to add -L to follow redirect (from nix website)
curl -L https://nixos.org/nix/install | sh
# Sometimes, the first installation fails, I hope two times fixes the issue
curl -L https://nixos.org/nix/install | sh
echo ". /home/g5k/.nix-profile/etc/profile.d/nix.sh" > /home/g5k/.bashrc
# Activate nix
source /home/g5k/.bashrc

# Install deps with nix
nix-env -iA nixpkgs.tcpkali nixpkgs.git

# Installing cachix
nix-env -iA cachix -f https://cachix.org/api/v1/install
cachix use g5k-interference

# Install experiment softs (MPI, mojitos etc)
# The nix script should have been deploied earlier in the script
nix-env -f ${SCRIPTS_FOLDER} -iA interference_host_env 2> /tmp/interference_host_env.err
EOF

#Finally, we set monitoring restriction to the lowest security level
ssh root@${HOST} 'echo -1 > /proc/sys/kernel/perf_event_paranoid'
