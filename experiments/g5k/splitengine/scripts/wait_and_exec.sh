#!/usr/bin/env bash

# Following thi blog post, it should kill child processes
# https://spin.atomicobject.com/2017/08/24/start-stop-bash-background-process/
trap "exit" INT TERM ERR
trap "kill 0" EXIT

>&2 echo "starts in $$"

# Get current epoch
current_epoch=$(date +%s)
# Date to wait for
target_epoch=$1

sleep_seconds=$(( $target_epoch - $current_epoch ))

# If the target epoch is not passed, we sleep
>&2 echo "starts in $sleep_seconds"
if [[ $sleep_seconds =~ ^[0-9]+$ ]]; then
   sleep $sleep_seconds
fi

${@:2} &
wait $!

