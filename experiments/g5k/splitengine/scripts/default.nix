{ hostPkgs ? import (fetchTarball {
  name = "pkgs";
  url = "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz";
  sha256 = "sha256:1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy";
}) { },
# Last working: f70b1149a9ae30fafe87af46e517985bacc331c3
kapack ? import (fetchTarball {
  name = "kapack";
  url = "https://github.com/oar-team/kapack/archive/master.tar.gz";
  sha256 = "sha256:1pfbkinkv8lm72bq4k3g479dx0a9jcicq1jfp3056n1q2ngl417d";
}) { } }:
with kapack;
let
  environments = rec {
    inherit kapack pkgs;

    gemmpi = pkgs.stdenv.mkDerivation rec {
      name = "gemmpi-${version}";
      version = "dev";

      src = pkgs.fetchgit {
        url = "https://gitlab.inria.fr/adfaure/gemmpi";
        rev = "e57ea2f87f5da5ab8f45bef41024cefe6a27ae4d";
        sha256 = "sha256-k8tP3z31dxzflkHZpf1DdIVhd9Z8q1qxmIqbJrHs2/A=";
      };

      nativeBuildInputs = with pkgs; [ clang openblas ninja ];

      buildInputs = with pkgs; [ kapack.openmpi simgrid pkgs.boost ];

      buildPhase = ''
        mpicc --version
        mpirun --version
        ninja
      '';

      installPhase = ''
        mkdir -p $out/bin
        cp -r gemmpi   $out/bin/
        cp -r igemmpi   $out/bin/
      '';

      meta = with pkgs.stdenv.lib; {
        longDescription = ''
          Performs distributed pdgemm, the algorithm is the direct apadtation of the outer product
          discribed in Parallel Algorithms (Chapman & Hall/CRC Numerical Analysis and Scientific Computing Series).
        '';
        description = ''
          Matrix multiplication benchmark on MPI.
        '';
        homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
        license = licenses.gpl3;
        platforms = platforms.unix;
        broken = false;
      };

    };

    interference_host_env = with pkgs;
      let
        pythonPackages = pkgs.python37Packages;
        python = pkgs.python37;
        periods = import (fetchTarball
          "https://gitlab.inria.fr/adfaure/periods/-/archive/master/periods-master.tar.gz")
          { };

      in pkgs.buildEnv {
        name = "interference_g5k";

        paths = with pkgs; [
          periods.periods
          kapack.openmpi
          gemmpi
          openblas
          mojitos
          (python3.withPackages (ps: with ps; with python3Packages; [ docopt ]))
        ];

        # propagatedBuildInputs = with pythonPackages; [ periods.periods ];

        meta = with stdenv.lib; {
          description = "";
          longDescription = ''
            Instance environment for the interference experiment on g5k.
          '';
          homepage = "https://gitlab.inria.fr/adfaure/batmet";
          license = licenses.gpl3;
          platforms = platforms.unix;
          broken = false;
        };
      };
  };
in environments

