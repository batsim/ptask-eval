#!/usr/bin/env bash
# FIXME: It looks like this script is not used anymore. I'll delete it when I am sure.

# Execute all argument as it were a bash command
nohup ${@:1}

# Save and echo PID
COMMAND_PID=$!
echo "${COMMAND_PID}"
