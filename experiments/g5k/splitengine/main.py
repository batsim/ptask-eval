#!/usr/bin/env python3

import copy
import datetime
import logging
import os
import random
import subprocess
import sys
import time
import traceback
from pathlib import Path

import click
import yaml

from .deployment import configure_network, setup_g5k_hosts
from .instance import Instance


def save_status(status, file):
    with open(file, "w") as outfile:
        yaml.safe_dump(status, outfile, default_flow_style=False)


@click.command()
@click.option(
    "--instance-file",
    default=None,
    help="Run a single instance",
)
@click.option(
    "--keys-folder",
    default=None,
    help="Folder containing the ssh key for the experiment.",
)
@click.option(
    "--isolate/--no-isolation",
    default=True,
    help="With --no-isolation the nodes will remains accessible during the experiment. However it can leads to uncontrolled behavior.",
)
@click.option(
    "--install/--skip-install",
    default=True,
    help="Skip node configuration (typically if it has already been done).",
)
@click.argument(
    "FILENAME",
)
@click.argument(
    "RESULTS-DIR",
)
def main(keys_folder, filename, results_dir, isolate, install, instance_file):
    """
    Runs the experiment described in FILENAME, and downloads the results to RESULTS_DIR.
    """
    logging.basicConfig(format="[%(asctime)s]::%(message)s", level=logging.INFO)
    logging.info("Starting experiment")
    logging.info("Network isolation:{}".format(isolate))

    status = dict()
    status["start_time"] = datetime.datetime.now()

    if keys_folder is None:
        keys_folder = "/tmp/experiment_keys"
        logging.info("no key folder provided, using default: {}".format(keys_folder))
    else:
        logging.info("Looking for ssh keys in {}".format(keys_folder))

    if results_dir is None:
        click.echo("No local folder specified")
        return 1

    logging.info("Create result dir: {}".format(results_dir))
    os.makedirs(results_dir)

    save_file = os.path.join(results_dir, "exp.{}.yaml".format(int(time.time())))

    # Security option for ssh configuration
    os.chmod(keys_folder + "/id_rsa", 0o400)
    os.chmod(keys_folder + "/id_rsa.pub", 0o600)

    experiment_file = filename
    with open(experiment_file) as f:
        # use safe_load instead load
        experiment_conf = yaml.safe_load(f)

        print(experiment_conf)

    job_id = os.environ["OAR_JOB_ID"]
    # Get the node list from oar environment variables
    nodes_list = subprocess.getoutput("cat $OAR_NODE_FILE|uniq").split("\n")

    if install:
        logging.info("Installing nodes")
        setup_g5k_hosts(nodes_list, job_id, keys_folder)
    else:
        logging.info("Skipping node installation")

    networks = configure_network(nodes_list, nodes_list)

    save_status(status, save_file)

    if isolate:
        logging.info("Isolate net group 1")
        networks[0].isolate_network()
        logging.info("Isolate net group 2")
        networks[1].isolate_network()

    if instance_file is not None:
        exp = Instance(
            instance_file, networks[0], networks[1], root_folder=local_folder
        )
        logging.info("Start instance: {}".format(exp.run_name))
        exp.run_instance()
    else:
        try:
            all_instances = []
            for instance in experiment_conf:
                filepath = os.path.join(
                    os.path.dirname(experiment_file), instance["file"]
                )
                if not os.path.exists(filepath):
                    logging.critical("instance file %s does not exist" % filepath)
                    sys.exit()

                instance["file"] = filepath

                for repeat in range(instance["repeat"]):
                    all_instances.append(copy.deepcopy(instance))

            random.Random(42).shuffle(all_instances)
            status["all_instances"] = all_instances

            total_finished = 0
            number_of_instances = len(all_instances)
            status["progress"] = f"{total_finished} / {number_of_instances}"

            for instance in all_instances:
                if not instance["groupname"] in status:
                    status[instance["groupname"]] = dict()
                    group = status[instance["groupname"]]

                save_status(status, save_file)
                local_folder = os.path.join(results_dir, instance["groupname"])

                Path("local_folder").mkdir(parents=True, exist_ok=True)

                exp = Instance(
                    instance["file"], networks[0], networks[1], root_folder=local_folder
                )
                logging.info("Start instance: {}".format(exp.run_name))

                if not exp.in_data["instance_name"] in group:
                    group[exp.in_data["instance_name"]] = dict()

                start = datetime.datetime.now()
                group[exp.in_data["instance_name"]][exp.run_name] = {
                    "status": "started",
                    "started_time": start,
                }

                save_status(status, save_file)
                logging.info("Executes instances {}".format(instance["groupname"]))
                exp.run_instance()

                end = datetime.datetime.now()
                group[exp.in_data["instance_name"]][exp.run_name]["status"] = "finished"
                group[exp.in_data["instance_name"]][exp.run_name][
                    "end_time"
                ] = datetime.datetime.now()
                group[exp.in_data["instance_name"]][exp.run_name]["duration"] = str(
                    end - start
                )

                total_finished += 1
                status["progress"] = f"{total_finished} / {number_of_instances}"
                save_status(status, save_file)

        except Exception as e:
            logging.info("Execption occured: {}".format(e))
            traceback.print_exc()

    if isolate:
        logging.info("Expose network")
        networks[0].expose_network()
        networks[1].expose_network()

    status["end_time"] = datetime.datetime.now()
    save_status(status, save_file)


if __name__ == "__main__":
    main()
