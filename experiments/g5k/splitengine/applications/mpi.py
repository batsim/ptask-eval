import copy
import logging
import os
import random
import subprocess
import time

from ..instance import Instance
from ..remote_process import RemoteProcess


class Mpi:
    def __init__(self, app_config, experiment: Instance, nodes_g1, nodes_g2):
        self.app_config = app_config
        # Application defined data
        self.mpi_data = copy.deepcopy(self.app_config["app_data"])
        # The nodes allocated
        self.nodes_g1 = nodes_g1
        self.nodes_g2 = nodes_g2

        self.g1_ips = experiment.group_1.get_corresponding_ips(self.nodes_g1)
        self.g2_ips = experiment.group_2.get_corresponding_ips(self.nodes_g2)

        # It look like an anti-pattern. Instance has a ref on Mpi, and so does Mpi...
        self.experiment = experiment
        # Generate the rankfile
        self.__generate_hostfile()
        self.__generate_rand_rankfile()
        self._info = {
            "app_config": self.app_config,
            "nodes_g1": self.nodes_g1,
            "nodes_g2": self.nodes_g2,
        }

        # Determining the node running mpi, so we can push hostfile and rankfile
        self.entry_host = self.nodes_g1[0]
        self.entry_ip = self.experiment.group_1.get_ip_for_host(self.entry_host)
        logging.info(
            "MPI: nodes running mpirun: {} <-> {}".format(
                self.entry_host, self.entry_ip
            )
        )
        # TODO: addr feels weird here
        addr = "g5k@%s" % self.entry_ip
        # Remote path (i.e. location on the experiment nodes) for hostfile and rankfile
        self.remote_path = "{}:{}".format(addr, experiment.remote_folder_name)
        logging.info("Init new {} application".format(self.__class__.__name__))

    def prepare(self):
        logging.info("Preparing MPI")
        self.hostfile_path = os.path.join(self.experiment.local_folder_name, "hostfile")
        self.rankfile_path = os.path.join(self.experiment.local_folder_name, "rankfile")

        with open(self.hostfile_path, "w") as outfile:
            outfile.write(self.hostfile)

        with open(self.rankfile_path, "w") as outfile:
            outfile.write(self.rankfile)

        logging.info("Node that runs mpi is : %s" % self.entry_host)

        proxy_jump = [
            "-o",
            'ProxyCommand="ssh -i {} -W %h:%p g5k@{}"'.format(
                self.experiment.key, self.experiment.router
            ),
        ]

        logging.info("Deploying hostfile")
        exec_array = (
            ["scp", "-i", self.experiment.key]
            + proxy_jump
            + [self.hostfile_path, self.remote_path]
        )
        logging.info(" ".join(exec_array))

        # TODO: warning about ssh man in the middle comes from here.
        # We could capture the output at least.
        subprocess.run(" ".join(exec_array), shell=True)

        logging.info("Deploying rankfile")
        exec_array = (
            ["scp", "-i", self.experiment.key]
            + proxy_jump
            + [self.rankfile_path, self.remote_path]
        )
        logging.info(" ".join(exec_array))
        subprocess.run(" ".join(exec_array), shell=True)

        if "mpi_args" not in self.mpi_data:
            mpi_args = ""
        else:
            mpi_args = self.mpi_data["mpi_args"]

        mpi_cmd = " ".join(
            [
                "mpirun",
                "-hostfile",
                os.path.join(self.experiment.remote_folder_name, "hostfile"),
                "--rankfile",
                os.path.join(self.experiment.remote_folder_name, "rankfile"),
                mpi_args,
                self.mpi_data["binary"],
                self.mpi_data["binary_args"],
                "-p {}".format(
                    os.path.join(self.experiment.remote_folder_name, "mpi.progress")
                ),
            ]
        )

        self._info.update({"command": mpi_cmd, "mpirun_on_host": self.entry_host})

        logfile = os.path.join(
            self.experiment.remote_folder_name, "{}.mpirun".format(self.entry_host)
        )

        p = RemoteProcess(
            self.entry_ip,
            mpi_cmd,
            gateway=self.experiment.router,
            remote_stderr=logfile + ".err",
            remote_stdout=logfile + ".out",
            access_key=self.experiment.key,
        )

        self.mpi_process = p

    def run(self, starts_at=None):

        p = self.mpi_process

        p.run_bg(starts_at=starts_at)

        self._info.update(
            {
                "runtime": str(p.get_runtime()),
                "start_time": str(p.get_starttime()),
                "end_time": str(p.get_endtime()),
                "reached_timeout": str(p.was_timeout_reached()),
            }
        )

    def wait(self, timeout):
        self.mpi_process.wait(timeout=timeout)

    def clean(self):
        self.stop()

    def stop(self):
        logging.info("clean mpi")
        self.experiment.execute_on_machines(
            self.g1_ips, "pkill mpirun", wait=False, use_router=True
        )
        self.experiment.execute_on_machines(
            self.g2_ips, "pkill mpirun", wait=False, use_router=True
        )

    def info(self):
        return self._info

    def __generate_hostfile(self):
        #  mpi_data = copy.deepcopy(self.experiment.in_data["mpi"])
        ip_list = []
        for host in self.nodes_g1:
            ip_list.append(self.experiment.group_1.get_ip_for_host(host))

        for host in self.nodes_g2:
            ip_list.append(self.experiment.group_2.get_ip_for_host(host))

        hostfile = ""
        for ip in ip_list:
            hostfile += "{} slots={}\n".format(ip, self.mpi_data["procs_per_host"])

        self.hostfile = hostfile

    def __generate_rand_rankfile(self):
        mpi_data = self.mpi_data
        ip_list = []
        for host in self.nodes_g2:
            ip_list.append(self.experiment.group_2.get_ip_for_host(host))

        for host in self.nodes_g1:
            ip_list.append(self.experiment.group_1.get_ip_for_host(host))

        rankfile = ""
        cores_list = []
        for ip in ip_list:
            for i in range(int(mpi_data["procs_per_host"])):
                cores_list.append(ip)

        random.Random(42).shuffle(cores_list)

        rank = 0
        ip_count = dict()
        for ip in cores_list:
            if ip not in ip_count:
                ip_count[ip] = 0
            rankfile += "rank {}={} slot={}\n".format(rank, ip, ip_count[ip])
            ip_count[ip] = ip_count[ip] + 1
            rank = rank + 1

        self.rankfile = rankfile

    def __generate_rand_hostfile(self):
        mpi_data = self.mpi_data
        ip_list = []
        for host in self.nodes_g2:
            ip_list.append(self.experiment.group_2.get_ip_for_host(host))

        for host in self.nodes_g1:
            ip_list.append(self.experiment.group_1.get_ip_for_host(host))

        hostfile = ""
        cores_list = []
        for ip in ip_list:
            # hostfile += "{} slots={}\n".format(ip, mpi_data["procs_per_host"])
            for i in range(int(mpi_data["procs_per_host"])):
                cores_list.append(ip)

        random.Random(42).shuffle(cores_list)
        hostfile = "\n".join(cores_list)
        self.hostfile = hostfile + "\n"
