import copy
import logging
import os

from ..instance import Instance
from ..remote_process import RemoteProcess


class TcpKali:
    def __init__(self, app_config, experiment: Instance, nodes_g1, nodes_g2):
        self.app_config = app_config
        # Application defined data
        self.app_data = copy.deepcopy(self.app_config["app_data"])
        # The nodes allocated
        self.nodes_g1 = nodes_g1
        self.nodes_g2 = nodes_g2
        # It look like an anti-pattern. Instance has a ref on Mpi, and so does Mpi...
        self.experiment = experiment
        self._info = {
            "app_config": self.app_config,
            "nodes_g1": self.nodes_g1,
            "nodes_g2": self.nodes_g2,
        }

        # Do we generate periods of activity / inactivity ?
        self.is_periodic = "periods" in self.app_data["client"]
        # Get the timeout of the experiment
        self.timeout = experiment.in_data["timeout"]

    def run(self, starts_at):
        self.tcpkali_processes_client.run_bg(starts_at=starts_at)
        self.tcpkali_processes_server.run_bg()

    def clean(self):
        logging.debug("Kill tcpkali")
        self.experiment.execute_on_machines(
            self.nodes_g1 + self.nodes_g2,
            "pkill --signal INT tcpkali",
            wait=False,
            use_router=True,
        )

        self.experiment.execute_on_machines(
            self.nodes_g2 + self.nodes_g2, "pkill periods", wait=False, use_router=True
        )

    def wait(self, timeout):
        if self.app_config["best_effort"]:
            pass
        else:
            raise NotImplementedError("Best effort not supported.")

    def prepare(self):
        self.__prepare_tcpkali_server()
        self.__prepare_tcpkali_client()

    def stop(self):
        self.tcpkali_processes_client.terminate(2)
        self.tcpkali_processes_server.terminate(2)

    def info(self):
        return copy.deepcopy(self._info)

    def __prepare_tcpkali_server(self):
        logging.info("Running tcpkali")
        ser_kalidata = copy.deepcopy(self.app_data["server"])

        server_command_tmpl = ["tcpkali"]
        for param in ser_kalidata:
            server_command_tmpl += ["--{}".format(param), str(ser_kalidata[param])]

        server_command_tmpl += [
            "--duration",
            "{}".format(self.timeout),
        ]

        ser_ip = self.experiment.group_2.get_ip_for_host(self.nodes_g2[0])
        ser_cmd = " ".join(server_command_tmpl)

        self._info["server_command"] = ser_cmd

        logging.info("tcpkali running")
        logfile = os.path.join(self.experiment.remote_folder_name, "{}.tcpkali")

        self.tcpkali_processes_server = RemoteProcess(
            ser_ip,
            ser_cmd,
            gateway=self.experiment.router,
            remote_stderr=logfile.format(self.nodes_g2[0]) + ".err",
            remote_stdout=logfile.format(self.nodes_g2[0]) + ".out",
            access_key=self.experiment.key,
        )

    def __prepare_tcpkali_client(self):
        logging.info("Running tcpkali")
        cli_kalidata = copy.deepcopy(self.app_data["client"])
        # ./periods tidle 5 tcmd 10 -e 1000.e -o 1000.o -g D -- tcpkali -m 0 127.0.0.1:8080 -T 5 --connections 10

        client_command_tmpl = ["tcpkali"]
        for param in cli_kalidata["args"]:
            if param != "port":
                client_command_tmpl += [
                    "--{}".format(param),
                    str(cli_kalidata["args"][param]),
                ]

        # Server host
        ser_ip = self.experiment.group_2.get_ip_for_host(self.nodes_g2[0])
        client_command_tmpl += ["{}:{}".format(ser_ip, cli_kalidata["args"]["port"])]

        cli_cmd = " ".join(client_command_tmpl)

        logging.info("tcpkali running")
        logfile = os.path.join(self.experiment.remote_folder_name, "{}.tcpkali")
        periodslogfile = os.path.join(self.experiment.remote_folder_name, "{}.periods")

        if self.is_periodic:
            logging.info("Configure periodic tcpkali")
            periodslogfile = os.path.join(
                self.experiment.remote_folder_name, "{}.periods"
            )
            periodic_command_tmpl = "periods tidle {tidle} tcmd {tcmd} -e {stderr} -o {stdout} --duration {duration} -g D -- {command}"
            final_cli_cmd = periodic_command_tmpl.format(
                tidle=cli_kalidata["periods"]["tidle"],
                tcmd=cli_kalidata["periods"]["tcmd"],
                stderr=logfile.format(self.nodes_g1[0]) + ".err",
                stdout=logfile.format(self.nodes_g1[0]) + ".out",
                command=cli_cmd,
                duration=self.timeout,
            )
        else:
            logging.info("tcpkali without period control")
            final_cli_cmd = cli_cmd

        self._info["client_command"] = final_cli_cmd
        cli_ip = self.experiment.group_1.get_ip_for_host(self.nodes_g1[0])

        self.tcpkali_processes_client = RemoteProcess(
            cli_ip,
            final_cli_cmd,
            gateway=self.experiment.router,
            remote_stdout=periodslogfile.format(self.nodes_g1[0]) + ".out",
            remote_stderr=periodslogfile.format(self.nodes_g1[0]) + ".err",
            access_key=self.experiment.key,
        )
