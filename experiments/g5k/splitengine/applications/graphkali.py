import copy
import functools
import logging
import os
import random
from itertools import count

from ..instance import Instance
from ..remote_process import RemoteProcess


def graph_and_weight(acc, n):
    acc[0].append((n["g1"], n["g2"]))
    acc[1].append(n["w"])
    return acc


class GraphKali:
    count_instances = 42

    def __init__(self, app_config, experiment: Instance, nodes_g1, nodes_g2):
        GraphKali.count_instances += 1
        # The config keeps the allocation
        self.app_config = app_config
        # Application defined data
        self.app_data = copy.deepcopy(self.app_config["app_data"])
        # The nodes allocated
        self.nodes_g1 = nodes_g1
        self.nodes_g2 = nodes_g2

        # Corresponding ips
        self.g1_ips = experiment.group_1.get_corresponding_ips(self.nodes_g1)
        self.g2_ips = experiment.group_2.get_corresponding_ips(self.nodes_g2)

        # It look like an anti-pattern. Instance has a ref on Mpi, and so does Mpi...
        self.experiment = experiment
        self._info = {
            "app_config": self.app_config,
            "nodes_g1": self.nodes_g1,
            "nodes_g2": self.nodes_g2,
        }

        # Do we generate periods of activity / inactivity ?
        self.is_periodic = "periods" in self.app_data["client"]
        # Get the timeout of the experiment
        self.timeout = experiment.in_data["timeout"]
        self.duration = int(self.app_data["duration"])

        self._info["server_commands"] = []
        self._info["client_commands"] = []

        # Array to keep client and server remote processes
        self.servers = []
        self.clients = []

    def run(self, starts_at):
        for process in self.servers + self.clients:
            process.run_bg(starts_at=starts_at)

    def clean(self):
        logging.info("clean tcpkali")
        self.experiment.execute_on_machines(
            self.g1_ips + self.g2_ips,
            "pkill --signal INT tcpkali",
            wait=False,
            use_router=True,
        )

        if self.is_periodic:
            self.experiment.execute_on_machines(
                self.g1_ips + self.g2_ips,
                "pkill periods",
                wait=False,
                use_router=True,
            )

        logging.info("graphkali host are cleaned")

    def wait(self, timeout):
        for process in self.servers + self.clients:
            logging.debug("wait for {} on {}".format(process.pid, process.hostname))
            process.wait(timeout)

    def prepare(self):
        self.seed = int(self.app_data["seed"])
        logging.info("RNG seed: {}".format(self.seed))
        # Seeded RNG
        self.rng = random.Random(self.seed)
        self.nb_connections = int(self.app_data["number_of_connections"])

        # Transforms the inputs to be fed into random.choices
        choices_input = functools.reduce(
            graph_and_weight, self.app_data["graph"], ([], [])
        )
        # This is the function generating a random graph
        self.generated_graph = list(
            self.rng.choices(choices_input[0], choices_input[1], k=self.nb_connections)
        )
        # logging.info("generated graph: {}".format(self.generated_graph))

        # Now we create a random mapping id -> host_name
        self.mapping_g1 = copy.deepcopy(self.nodes_g1)
        self.mapping_g2 = copy.deepcopy(self.nodes_g2)

        # We reset the generator so mapping_g1 and mapping_g2
        # are randomized the same way.
        # The nodes of the graph `g1: 0` for instance can be accessed
        # using the mapping lists such as self.mapping_g1[0]
        random.Random(self.seed).shuffle(self.mapping_g1)
        random.Random(self.seed).shuffle(self.mapping_g2)

        # Register the mapping for the log
        mappings = {"g1": [], "g2": []}

        for i in range(len(self.mapping_g1)):
            mappings["g1"].append({"id": i, "node": self.mapping_g1[i]})
            mappings["g2"].append({"id": i, "node": self.mapping_g2[i]})

        self._info.update({})

        # Array to register association client / server
        associations = {}

        # Generated graph is a list of tuples (x, y)
        # where x and y represent a node of the application.
        # Because the nodes are randomized,
        # we use the mapping to get the corresponding nodes.
        logging.info("Preparing clients and servers")
        port = 8080
        for cli_and_ser in self.generated_graph:
            # Get the corresponding node according to a randomly
            # generated mapping
            cli_node = self.mapping_g1[cli_and_ser[0]]
            ser_node = self.mapping_g2[cli_and_ser[1]]

            # Log association
            assoc = (cli_node, ser_node)
            if not assoc in associations:
                associations[assoc] = {
                    "g1": cli_node,
                    "g2": ser_node,
                    "total": 1,
                    "port_list": [port],
                }
            else:
                associations[assoc]["total"] += 1
                associations[assoc]["port_list"].append(port)

            # Generates the processes
            self.clients.append(self.__prepare_tcpkali_client(cli_node, ser_node, port))
            self.servers.append(self.__prepare_tcpkali_server(ser_node, port))
            port += 1

        self._info.update(
            {
                "connections": [v for k, v in associations.items()],
                "mappings": mappings,
                "seed": self.seed,
            }
        )

    def stop(self):
        self.clean()
        # for process in self.servers + self.clients:
        #     # With sig INT(2) tcp kali writes data in output
        #     process.terminate(2)

    def info(self):
        return copy.deepcopy(self._info)

    def __prepare_tcpkali_server(self, server_node, listen_port):
        logging.debug("Prepare tcpkali server on {}".format(server_node))
        ser_kalidata = copy.deepcopy(self.app_data["server"])

        server_command_tmpl = ["tcpkali"]
        for param in ser_kalidata:
            server_command_tmpl += ["--{}".format(param), str(ser_kalidata[param])]

        server_command_tmpl += ["--listen-port", str(listen_port)]

        server_command_tmpl += [
            "--duration",
            "{}".format(self.duration),
        ]

        #  Be carefull, servers mus be in group 2 or it wont work
        ser_ip = self.experiment.group_2.get_ip_for_host(server_node)
        ser_cmd = " ".join(server_command_tmpl)

        self._info["server_commands"].append(ser_cmd)

        #  logging.info("tcpkali running")
        logfile = os.path.join(
            self.experiment.remote_folder_name, "{}.{}.tcpkali.server"
        )

        return RemoteProcess(
            ser_ip,
            ser_cmd,
            gateway=self.experiment.router,
            remote_stderr=logfile.format(server_node, listen_port) + ".err",
            remote_stdout=logfile.format(server_node, listen_port) + ".out",
            access_key=self.experiment.key,
        )

    def __prepare_tcpkali_client(self, client_node, server_node, port):
        logging.debug(
            "Prepare tcpkali client {} to server {}".format(client_node, server_node)
        )
        cli_kalidata = copy.deepcopy(self.app_data["client"])
        # ./periods tidle 5 tcmd 10 -e 1000.e -o 1000.o -g D -- tcpkali -m 0 127.0.0.1:8080 -T 5 --connections 10

        client_command_tmpl = ["tcpkali"]
        for param in cli_kalidata["args"]:
            if param != "port":
                client_command_tmpl += [
                    "--{}".format(param),
                    str(cli_kalidata["args"][param]),
                ]

        port_number = port
        # Server host
        ser_ip = self.experiment.group_2.get_ip_for_host(server_node)
        client_command_tmpl += ["{}:{}".format(ser_ip, port_number)]

        cli_cmd = " ".join(client_command_tmpl)

        if self.is_periodic:
            logfile = os.path.join(self.experiment.remote_folder_name, "{}.{}.periods")
            # teels the script periods where to log tcpkali
            tcpkalilog = os.path.join(
                self.experiment.remote_folder_name, "{}.{}.tcpkali.client"
            )

            logging.debug("Configure periodic tcpkali")
            periodslogfile = os.path.join(
                self.experiment.remote_folder_name, "{}.{}.periods"
            )
            periodic_command_tmpl = "periods tidle {tidle} tcmd {tcmd} -e {stderr} -o {stdout} --duration {duration} -g D -- {command}"
            final_cli_cmd = periodic_command_tmpl.format(
                tidle=cli_kalidata["periods"]["tidle"],
                tcmd=cli_kalidata["periods"]["tcmd"],
                stderr=tcpkali.format(client_node, port) + ".err",
                stdout=tcpkali.format(client_node, port) + ".out",
                command=cli_cmd,
                duration=self.duration,
            )
        else:
            logfile = os.path.join(
                self.experiment.remote_folder_name, "{}.{}.tcpkali.client"
            )
            logging.debug("tcpkali without period control")
            final_cli_cmd = cli_cmd

        self._info["client_commands"].append(final_cli_cmd)
        cli_ip = self.experiment.group_1.get_ip_for_host(client_node)

        return RemoteProcess(
            cli_ip,
            final_cli_cmd,
            gateway=self.experiment.router,
            remote_stdout=logfile.format(client_node, port) + ".out",
            remote_stderr=logfile.format(client_node, port) + ".err",
            access_key=self.experiment.key,
        )
