import datetime
import logging
import subprocess
import time
from subprocess import PIPE


class RemoteProcess:
    def __init__(
        self,
        hostname,
        command,
        remote_stderr=None,
        remote_stdout=None,
        gateway=None,
        extra_ssh_opts=[],
        access_key=None,
    ):

        self.hostname = hostname
        # self.pid = pid
        self.remote_stderr = remote_stderr
        self.remote_stdout = remote_stdout
        self.access_key = access_key
        self.command = command
        self.program = command.split()[0]
        self.extra_ssh_opts = extra_ssh_opts
        # Using a gateway
        self.gateway = gateway
        if self.gateway is not None:
            self.proxy_jump = [
                "-o",
                'ProxyCommand="ssh -i {} -W %h:%p g5k@{}"'.format(
                    self.access_key, self.gateway
                ),
            ]

        # Only for self.run()
        self.runtime = None
        self.ends_at = None
        self.starts_at = None
        self.stdout = None
        self.stderr = None
        self.timeout_reached = False
        self.is_started = False
        self.is_finished = False
        self.run_time = 0

    def get_ssh_array(self, extra_ssh_opts=[]):
        # TODO: Specify know host to avoid ssh warning: https://stackoverflow.com/a/10765980
        if self.gateway is not None:
            process_array = (
                ["ssh", "-i", self.access_key]
                + extra_ssh_opts
                + self.extra_ssh_opts
                + self.proxy_jump
                + ["g5k@%s" % self.hostname]
            )
        else:
            process_array = (
                ["ssh", "-i", self.access_key]
                + extra_ssh_opts
                + self.extra_ssh_opts
                + ["g5k@%s" % self.hostname]
            )

        return process_array

    def wait(self, timeout=None, starts_at=None):
        if self.is_finished:
            return self

        if not self.is_started:
            logging.debug("process not running, lauching")
            try:
                self.run_bg(timeout=timeout)
            except subprocess.TimeoutExpired as e:
                logging.debug("popen timeout expired: {}".format(e))
                self.timeout_reached = True
                return self

        if not self.check():
            self.is_finished = True
            self.ends_at = datetime.datetime.now()
            self.run_time = self.ends_at - self.starts_at
            return self

        if timeout is not None:
            elapsed = datetime.datetime.now() - self.starts_at
            remaining = timeout - elapsed.seconds

            if elapsed.seconds > timeout and self.check():
                logging.info("timeout reached anyway")
                self.terminate()
                self.timeout_reached = True
            else:
                logging.info("program running, remaining seconds %s " % remaining)

                process_array = self.get_ssh_array() + [
                    "'timeout {} tail --pid={} -f /dev/null'".format(
                        remaining, self.pid
                    )
                ]

                result = subprocess.run(
                    " ".join(process_array), shell=True, stdout=PIPE, stderr=PIPE
                )

                self.terminate()

                if result.returncode == 124 or result.returncode == 137:
                    self.timeout_reached = True
                else:
                    self.timeout_reached = False

        else:
            # Wait for the end of the process using tail
            process_array = self.get_ssh_array() + [
                "tail --pid={} -f /dev/null".format(self.pid)
            ]
            result = subprocess.run(
                " ".join(process_array), shell=True, stdout=PIPE, stderr=PIPE
            )

            self.is_finished = True
            self.ends_at = datetime.datetime.now()
            self.run_time = self.ends_at - self.starts_at

        return self

    def run(self, timeout=None, starts_at=None):
        # print(" ".join(["ssh", "-i", self.key, "g5k@%s" % self.node, nohuped]))
        if self.is_started or self.is_finished:
            logging.error("process already finished or running")
            return

        if timeout is not None:
            return self.wait(timeout=timeout)

        if starts_at is not None:
            # The setup script should put the script file in /home/g5k/.scripts
            wae = "/home/g5k/.scripts/wait_and_exec.sh " + str(starts_at) + " "

            self.command = wae + self.command

        command = self.command
        if self.remote_stdout is not None:
            command += " > %s " % self.remote_stdout
        if self.remote_stderr is not None:
            command += " 2> %s " % self.remote_stderr

        self.is_started = True
        self.starts_at = datetime.datetime.now()

        process_array = self.get_ssh_array() + ["'" + command + "'"]

        # logging.info("run: {}".format(process_array))
        result = subprocess.run(
            [" ".join(process_array)], shell=True, stdout=PIPE, stderr=PIPE
        )

        self.ends_at = datetime.datetime.now()
        self.run_time = self.ends_at - self.starts_at

        rpid = result.stdout.decode().rstrip()
        self.pid = rpid
        self.is_finished = True
        return self

    def get_stdout(self):

        process_array = self.get_ssh_array() + ["cat {}".format(self.remote_stdout)]

        result = subprocess.run(
            " ".join(process_array), shell=True, stdout=PIPE, stderr=PIPE
        )

        return result.stdout.decode("utf-8").strip()

    def get_endtime(self):
        return self.ends_at

    def get_starttime(self):
        return self.starts_at

    def get_runtime(self):
        return self.run_time

    def check(self):

        # This function has a terrible design.
        # It checks if the remote process is still running by sending sig = 0
        # This hos for effect to do nothing on succes, or send an error if the
        # process does not exist.
        # The main issue with this function is that if ssh has errors,
        # it is likely to have log into the stderr.
        # As a workaround, I redirect the error of the kill cmd into the stout
        process_array = self.get_ssh_array() + ["'kill -s 0 %s 2>&1'" % self.pid]

        result = subprocess.run(
            " ".join(process_array), shell=True, stdout=PIPE, stderr=PIPE
        )

        logging.debug(
            self.pid,
            "out {}: ".format(len(result.stdout)),
            result.stdout.decode(),
            self.pid,
            "\nerr",
            result.stderr.decode(),
        )

        return len(result.stdout) == 0

    def was_timeout_reached(self):
        return self.timeout_reached

    def run_bg(self, starts_at=None, timeout=None):
        if self.is_started or self.is_finished:
            logging.error("process already finished or running")
            return self

        if starts_at is not None:
            wae = "/home/g5k/.scripts/wait_and_exec.sh " + str(starts_at) + " "
            self.command = wae + self.command

        nohuped = self.wrap_nohup(
            self.command, stdout=self.remote_stdout, stderr=self.remote_stderr
        )

        process_array = self.get_ssh_array() + [nohuped]

        self.starts_at = datetime.datetime.now()

        logging.debug(
            "starting remote process on bg: {}".format(" ".join(process_array))
        )
        result = subprocess.run(
            " ".join(process_array),
            shell=True,
            stdout=PIPE,
            stderr=PIPE,
            timeout=timeout,
        )

        rpid = result.stdout.decode().rstrip()
        self.pid = rpid
        self.is_started = True
        return self

    def send_sig(self, sig):
        if self.gateway is not None:
            process_array = (
                ["ssh", "-i", self.access_key]
                + self.proxy_jump
                + ["g5k@%s" % self.hostname, "kill -%d %s" % (sig, self.pid)]
            )

        else:
            process_array = [
                "ssh",
                "-i",
                self.access_key,
                "g5k@%s" % self.hostname,
                "kill -%d %s" % (sig, self.pid),
            ]

        res = subprocess.run(" ".join(process_array), shell=True, capture_output=True)
        logging.debug(
            "send sig: to {} - res".format(self.pid, res.stdout.decode().rstrip())
        )
        return self

    def kill(self):
        # should not be used because it wont kill child processes
        self.send_sig(9)

        if self.check() is True:
            logging.warning("failed to kill %s" % self.pid)
            return self

        self.is_finished = True
        self.ends_at = datetime.datetime.now()
        self.run_time = self.ends_at - self.starts_at
        return self

    def terminate(self, sig=15):
        self.send_sig(sig)
        # Sleep some time the process has time to exit normally
        time.sleep(1)
        if self.check() is True:
            logging.warning(
                "failed to send signal sig: {} to pid {}".format(sig, self.pid)
            )
            return self

        self.is_finished = True
        self.ends_at = datetime.datetime.now()
        self.run_time = self.ends_at - self.starts_at
        return self

    @staticmethod
    def wrap_nohup(command, stderr="/dev/null", stdout="/dev/null"):
        # IDK why, but we need to wrap it with two ' to make it works
        # return "''nohup %s </dev/null >/dev/null 2>&1 & echo $!''" % command
        if stderr is None:
            stderr = "/dev/null"

        if stdout is None:
            stdout = "/dev/null"

        return "'nohup %s </dev/null > %s 2> %s & echo $!'" % (command, stdout, stderr)
