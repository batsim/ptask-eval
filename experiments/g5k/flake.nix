{
  inputs = {
    # https://github.com/nix-community/poetry2nix/issues/279
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    poetry2nix-src.url = "github:nix-community/poetry2nix";
  };

  outputs = { nixpkgs, utils, poetry2nix-src, self }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ poetry2nix-src.overlay ];
        };
        splitengineEnv = pkgs.poetry2nix.mkPoetryEditablePackage {
          python = pkgs.python37;
          projectDir = ./.;
          editablePackageSources = { splitengine = ./splitengine; };
        };
      in {
        devShell = pkgs.mkShell {
          buildInputs = [ splitengineEnv pkgs.poetry pkgs.openssl.dev pkgs.dhall pkgs.dhall-json ];
          LD_LIBRARY_PATH = "${pkgs.openssl.out}/lib";
        };
      });
}
