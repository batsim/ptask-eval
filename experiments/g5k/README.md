# Run the real Experiment on Grid5000

## Create a job

To create a job, on the rennes frontend:

```bash
oarsub -I -l slash_22=2+{"virtual!='NO' AND cluster='paravance'"},/switch=1/nodes=3,walltime=2  -t deploy
oarsub -I -l slash_22=2+{"virtual!='NO' AND cluster='grisou'"},/switch=1/nodes=3,walltime=2  -t deploy
```

The parameter `-l slash_22=2+{"virtual!='NO' AND cluster='grisou'"}` enables to allocate a subnetwork.

Once the job started:

```bash
kadeploy3 -f $OAR_NODE_FILE -e debian10-x64-base -k
```

The start point for the experiment is the script: `main.py`.
The only dependency is the library PyYAML (3.13).

ssh needs to be configured on your home on the frontend.
It not a good idea to keep the configuration like this at it disable security check.

```sh
cat ~/.ssh/config

Host *
  StrictHostKeyChecking no
```


```
ssh -i experiment_keys/id_rsa -o ProxyCommand="ssh -i experiment_keys/id_rsa -W %h:%p g5k@parasilo-1.rennes.grid5000.fr" g5k@10.158.20.3
```

Trac MPI binding:
Report  `-report-bindings`

## Some configuration before you start

Create a set of ssh keys that will be deployed and use during the experiment (it can be done only once).

```
mkdir /tmp/exp_keys && ssh-keygen -f /tmp/exp_keys/id_rsa
```

## Describe an experiment

An experiment is described in a yaml, and contains a set of instances to do.
- An instance is a link to an instance yaml file.
- A group name (useful to sort the outputs).
- A number of repetitions.
The experiment generates all instances and randomize their execution order (to avoid biases such as warm-up effects).

An instance is describe in a yaml files, and contains a information about what will be executed during the instance (a set of application).

The files `experiment_example.yaml` and `instance_example.yaml` contains working (but simple examples).

## Run the python experiment

This folder contains the base code to do experiment about g5k, tcp and monitoring with MPI.

The project uses poetry, install with:

```bash
$ poetry install
```

and then use with

```bash
$ poetry shell
$ splitengine --help

Usage: splitengine [OPTIONS] FILENAME RESULTS_DIR

  Runs the experiment described in FILENAME, and downloads the results to
  RESULTS_DIR.

Options:
  --keys-folder TEXT          Folder containing the ssh key for the
                              experiment.

  --isolate / --no-isolation  With --no-isolation the nodes will remains
                              accessible during the whole experiment. However
                              it can leads to uncontrolled behavior.

  --install / --skip-install  Skip node configuration (typically if it has
                              already been done).

  --help                      Show this message and exit.
```

## Tips and tricks

### Debugging an experiment

Across several run of the same experiment (for debugging for instance), it is quicker to not install each time the hosts, and to not isolate the network (so it will remain reachable in case of failures).
Additionally, To store the result, the engine only get the base folder and then organizes folder according to their experiment names.

You can use this command:
```
splitengine --skip-install --no-isolation --keys-folder ./test_keys  experiment_example.yaml ~/batmet_experiments/$(date +%m-%d-%y:%T)
```

- `--skip-install` will skip the installation of the dependencies and the hosts configuration (it needs to be done at least one time thouhg).
- `--no-isolation`the network will remain accessible during the experiment. You can therefore directly connect to nodes with `ssh -i ./test_keys/id_rsa g5k@hostname`.
- The last argument `~/batmet_experiments/$(date +%m-%d-%y:%T)` simply gives the date as base folder so experiments are kept organized.

### SSH warnings

If ssh yells at you about man-in-the-middle attacks you can just clear your `mv ~/.ssh/known_hosts ~/.ssh/known_hosts.bak`.
