import os, sys, glob, json, re
import analysis_splitengine as analysis

base_folder = sys.argv[1]

all_results = []
header_printed = False

TCPKALI_PARSE_REGEX = re.compile(
    r"^Aggregate bandwidth: (\d+(?:\.\d+)?)↓, (\d+(?:\.\d+)?)↑ ([kKM])([bB])ps$"
)


def parse_tcpkali_output(output):
    """Parse a tcpkali output string.
    Returns a (upload_bandwidth, download_bandwith) tuple with MBps units"""
    for line in output.split("\n"):
        m = TCPKALI_PARSE_REGEX.match(line.strip())
        if m:
            print(m.groups())
            upload, download, prefix, unit = m.groups()

            multiplier = None
            if prefix.lower() == "k":
                multiplier = 1e-3
            elif prefix == "M":
                multiplier = 1e0

            if unit == "b":
                multiplier = multiplier / 8

            return (float(upload) * multiplier, float(download) * multiplier)


def parse_tcpkali_output_file(filename):
    """Parse a tcpkali output file.
    Returns a (upload_bandwidth, download_bandwith) tuple with MBps units"""
    with open(filename, "r") as file:
        return parse_tcpkali_output(file.read())


summary_file = os.path.join(base_folder, "summary.csv")
tcp_file = os.path.join(base_folder, "tcpkali.csv")

with open(summary_file, "w") as f1, open(tcp_file, "w") as f2:
    for instances_dir in glob.glob(base_folder + "/*"):
        print("Instance: ", instances_dir)
        instances = analysis.Instances(instances_dir)

        for instance_name in instances.instances:
            instance = instances.instances[instance_name]
            if not header_printed:
                header_printed = True
                f1.write(
                    ",".join(
                        [
                            str(x[0])
                            for x in instance.info.items()
                            if x[0] not in ["out_data", "instance_folder"]
                        ]
                    )
                    + "\n"
                )
                f2.write(
                    "uuid,Server Host,Client host,port,Server upload (Mbps),Client upload (Mbps),Server Downloads (Mbps),Client Downloads (Mbps)\n"
                )

            all_results.append(instance_name)
            name_params = instance_name.split(".")
            if "alone" in name_params[1]:
                name_params.append("No_graph")

            f1.write(
                ",".join(
                    [
                        str(x[1])
                        for x in instance.info.items()
                        if x[0] not in ["out_data", "instance_folder"]
                    ]
                )
                + "\n"
            )

            if len(instance.info["out_data"]["be_applications"]) > 0:
                connections = instance.info["out_data"]["be_applications"][0][
                    "connections"
                ]
                for connection in connections:
                    for port in connection["port_list"]:
                        ser_file = "{hostname}.{port}.tcpkali.server.out".format(
                            hostname=connection["g2"], port=port
                        )
                        cli_file = "{hostname}.{port}.tcpkali.client.out".format(
                            hostname=connection["g1"], port=port
                        )

                        ser_output = parse_tcpkali_output_file(
                            os.path.join(instance.instance_folder, ser_file)
                        )
                        cli_output = parse_tcpkali_output_file(
                            os.path.join(instance.instance_folder, cli_file)
                        )

                        f2.write(
                            "{uuid},{ser_host},{cli_host},{port},{ser_up},{cli_up},{ser_down},{cli_down}".format(
                                uuid=instance.info["uuid"],
                                ser_host=connection["g2"],
                                cli_host=connection["g1"],
                                port=port,
                                ser_up=ser_output[0],
                                ser_down=ser_output[1],
                                cli_up=cli_output[0],
                                cli_down=cli_output[1],
                            )
                            + "\n"
                        )

print(summary_file)
print(tcp_file)
