import itertools
import os
import subprocess
import sys
from pathlib import Path

import dhall
import yaml

template_mpi_alone = """
let app = ./tools/dhall/applications.dhall

let graphkali = ./tools/dhall/graphkali_experiment_base.dhall

in  app.makeTypedInstance
      "{name}"
      [ graphkali.makeMpiApplication "{application}" ]
"""

template = """
let List/map =
      https://prelude.dhall-lang.org/v20.1.0/List/map sha256:dd845ffb4568d40327f2a817eb42d1c6138b929ca758d50bc33112ef3c885680

let List/zip =
      https://prelude.dhall-lang.org/v20.1.0/List/zip sha256:85ed955eabf3998767f4ad2a28e57d40cd4c68a95519d79e9b622f1d26d979da

let app = ./tools/dhall/applications.dhall

let graphkali = ./tools/dhall/graphkali_experiment_base.dhall

let node_type
    : Type
    = {{ g1 : Natural, g2 : Natural, w : Natural }}

let graph = {graph}
let weights = {weights}

let weightedGraph
    : app.RngGraph
    = let t
          : Type
          = {{ _1 : Natural, _2 : {{ g1 : Natural, g2 : Natural }} }}

      let zipped =
            List/zip Natural weights {{ g1 : Natural, g2 : Natural }} graph

      in List/map
            t
            node_type
            ( λ(element : t) →
                {{ g1 = element._2.g1, g2 = element._2.g2, w = element._1 }}
            )
            zipped

in  app.makeTypedInstance
      "{name}"
      [ graphkali.makeMpiApplication "{application}"
      , graphkali.makeGraphKaliApp (weightedGraph : app.RngGraph) {nb_connections} {seed}
      ]
"""

# Parameters
graph_A = (
    "GraphA",
    """[ { g1 = 0, g2 = 0 }
, { g1 = 1, g2 = 1 }
, { g1 = 2, g2 = 2 }
, { g1 = 3, g2 = 3 }
, { g1 = 4, g2 = 4 }
]
""",
)

graph_B = (
    "GraphB",
    """[ { g1 = 0, g2 = 0 }
, { g1 = 1, g2 = 0 }
, { g1 = 2, g2 = 0 }
, { g1 = 3, g2 = 0 }
, { g1 = 4, g2 = 0 }
]
""",
)

graph_C = (
    "GraphC",
    """[ { g1 = 0, g2 = 0 }
, { g1 = 0, g2 = 1 }
, { g1 = 0, g2 = 2 }
, { g1 = 0, g2 = 3 }
, { g1 = 0, g2 = 4 }
]
""",
)

connections = [2, 4, 8, 12, 16, 24, 32]
graphs = [graph_A, graph_B]
applications = ["gemmpi"]
weights = [
    ("balanced", [1, 1, 1, 1, 1]),
    # ("+1", [1, 2, 3, 4, 5]),
    ("x4", [1, 4, 8, 16, 32]),
]
seeds = [3, 4]

# Folder to generate files
folder = sys.argv[1]

if not os.path.exists(folder):
    Path(folder).mkdir(parents=True, exist_ok=False)

# Case without tcpkali at all
for app in applications:
    name = f"{app}.alone"
    data = template_mpi_alone.format(
        name=name,
        application=app,
    )
    load = dhall.loads(data)
    with open(f"{folder}/{name}.yaml", "w") as outfile:
        yaml.dump(load, outfile, default_flow_style=False)

# Case without only one tcp connection
for app in applications:
    name = f"{app}.1Tcp"
    data = template.format(
        name=name,
        nb_connections=str(1),
        graph=graph_A[1],  # Doesnt matter
        weights=str([1, 1, 1, 1, 1]),
        seed=str(1),
        application=app,
    )
    load = dhall.loads(data)
    with open(f"{folder}/{name}.yaml", "w") as outfile:
        yaml.dump(load, outfile, default_flow_style=False)

# The python dump of dhall generates illformatted dhall, so I use the real one instead
# p = subprocess.run(["dhall"], stdout=subprocess.PIPE, input=data, encoding="utf8")

# Cartesian product of all parameters
for element in itertools.product(graphs, connections, weights, seeds, applications):

    graph = element[0][1]
    graph_name = element[0][0]

    nb_connections = element[1]

    graph_weights = element[2][1]
    w_name = element[2][0]

    seed = element[3]

    application = element[4]

    name = f"{application}.{graph_name}.{w_name}.{nb_connections}Tcp.{seed}seed"

    data = template.format(
        name=name,
        nb_connections=nb_connections,
        graph=graph,
        weights=str(graph_weights),
        seed=seed,
        application=application,
    )

    # The python dump of dhall generates illformatted dhall, so I use the real one instead
    # p = subprocess.run(["dhall"], stdout=subprocess.PIPE, input=data, encoding="utf8")

    load = dhall.loads(data)
    with open(f"{folder}/{name}.yaml", "w") as outfile:
        yaml.dump(load, outfile, default_flow_style=False)
