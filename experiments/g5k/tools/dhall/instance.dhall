{- Number of resources from each group that the an `Application` needs. -}
let Allocation
    : Type
    = { hosts_per_group : { g1 : Natural, g2 : Natural } }

let Application
    : ∀(user_type : Type) → Type
    = λ(user_type : Type) →
        { type : Text
        , best_effort : Bool
        , allocation : Allocation
        , app_data : user_type
        }

let Mojitos
    : Type
    = { network : List Text, freq : Natural, performances_counters : Text }

let Instance
    : ∀(user_type : Type) → Type
    = λ(user_type : Type) →
        { instance_name : Text
        , timeout : Natural
        , mojitos : Mojitos
        , applications : List (Application user_type)
        }

let makeAllocation
    : Natural → Natural → Allocation
    = λ(g1 : Natural) → λ(g2 : Natural) → { hosts_per_group = { g1, g2 } }

let makeApplication
    : Text →
      Bool →
      Allocation →
      ∀(user_type : Type) →
      user_type →
        Application user_type
    = λ(type : Text) →
      λ(best_effort : Bool) →
      λ(allocation : Allocation) →
      λ(user_type : Type) →
      λ(data : user_type) →
        { type, best_effort, allocation, app_data = data }

let makeInstance
    : Text →
      ∀(user_type : Type) →
      List (Application user_type) →
        Instance user_type
    = λ(name : Text) →
      λ(user_type : Type) →
      λ(applications : List (Application user_type)) →
        let mojitos
            : Mojitos
            = { network = [ "eno1", "eno2" ]
              , freq = 1
              , performances_counters = "cpu_cycles,instructions"
              }

        let timeout = 5000

        in  { instance_name = name, timeout, mojitos, applications }

in  { makeInstance
    , makeAllocation
    , makeApplication
    , Allocation
    , Application
    , Instance
    }
