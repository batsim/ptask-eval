let instance = ./instance.dhall

let TcpKaliData
    : Type
    = { server :
          { message : Text
          , duration : Natural
          , listen-port : Text
          , listen-mode : Text
          }
      , client :
          { args : { message : Text, connections : Natural, duration : Natural }
          }
      }

let MpiData
    : Type
    = { mpi_args : Text
      , procs_per_host : Natural
      , binary : Text
      , binary_args : Text
      , nb_hosts : Natural
      , hosts_per_group : Natural
      }

let RngGraph
    : Type
    = List { g1 : Natural, g2 : Natural, w : Natural }

let GraphKaliData
    : Type
    = { duration : Natural
      , number_of_connections : Natural
      , graph : RngGraph
      , seed : Natural
      , server : { message : Text, listen-mode : Text }
      , client : { args : { message : Text, connections : Natural, duration: Natural } }
      }

let AppData
    : Type
    = < Mpi : MpiData | TcpKali : TcpKaliData | GraphKali : GraphKaliData >

let makeTypedApplication
    : Text → Bool → instance.Allocation → AppData → instance.Application AppData
    = λ(type : Text) →
      λ(best_effort : Bool) →
      λ(allocation : instance.Allocation) →
      λ(app_data : AppData) →
        instance.makeApplication type best_effort allocation AppData app_data

let makeTypedInstance
    : Text → List (instance.Application AppData) → instance.Instance AppData
    = λ(groupname : Text) →
      λ(applications : List (instance.Application AppData)) →
        instance.makeInstance groupname AppData applications

in  { AppData, RngGraph, makeTypedInstance, makeTypedApplication }
