let instance = ./instance.dhall

let app = ./applications.dhall

let makeMpiApplication
    : Text → instance.Application app.AppData
    = λ(binary : Text) →
        app.makeTypedApplication
          "mpi"
          False
          (instance.makeAllocation 8 8)
          ( app.AppData.Mpi
              { mpi_args =
                  "--report-bindings --bind-to core --mca btl self,vader,tcp --mca btl_tcp_if_include eno2 --mca coll_tuned_bcast_algorithm 5"
              , procs_per_host = 16
              , nb_hosts = 16
              , hosts_per_group = 8
              , binary
              , binary_args = "80000 -s 50 -B 1 -l 200"
              }
          )

let makeGraphKaliApp
    : app.RngGraph → Natural → Natural -> instance.Application app.AppData
    = λ(graph : app.RngGraph) →
      λ(number_of_connections : Natural) →
      \(seed: Natural) ->
        let timeout = 2500
        let server = { message = "m", listen-mode = "active" }

        let client = { args = { message = "m", connections = 1, duration = timeout } }

        in  app.makeTypedApplication
              "graphkali"
              True
              (instance.makeAllocation 5 5)
              ( app.AppData.GraphKali
                  { duration = timeout
                  , number_of_connections
                  , graph
                  , seed
                  , server
                  , client
                  }
              )

let mkNode
    : Natural → Natural → Natural → { g1 : Natural, g2 : Natural, w : Natural }
    = λ(g1 : Natural) → λ(g2 : Natural) → λ(w : Natural) → { g1, g2, w }

in  { mkNode, makeGraphKaliApp, makeMpiApplication }
