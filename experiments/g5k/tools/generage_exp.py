import glob
import os
import sys

import yaml

folder = sys.argv[1]

group_name_prefix = ""
if len(sys.argv) == 3:
    group_name_prefix = sys.argv[2]

folder_base = os.path.basename(folder)
files = glob.glob("{}/*.yaml".format(folder))

exp = []
for file in files:
    inst = dict()
    inst["groupname"] = (
        group_name_prefix + os.path.splitext(os.path.basename(folder))[0]
    )
    inst["file"] = os.path.join(folder_base, os.path.basename(file))
    inst["repeat"] = 2
    exp.append(inst)

print(yaml.dump(exp))
