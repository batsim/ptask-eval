{ pkgs, stdenv, openmpi, gemmpi, simgrid, mojitos, ... }:
with pkgs;
let
  pythonPackages = pkgs.python37Packages;
  python = pkgs.python37;
  periods = import (fetchTarball "https://gitlab.inria.fr/adfaure/periods/-/archive/master/periods-master.tar.gz") {};
in
pkgs.buildEnv {
  name = "interference_g5k";

  paths = with pkgs; [
    periods.periods
    openmpi
    gemmpi
    openblas
    mojitos
    (python3.withPackages (ps: with ps; with python3Packages; [ docopt ]))
  ];

  # propagatedBuildInputs = with pythonPackages; [ periods.periods ];

  meta = with stdenv.lib; {
    description = ''
    '';
    longDescription = ''
      Experiment environment for the interference experiment on g5k.
    '';
    homepage = "https://gitlab.inria.fr/adfaure/batmet";
    license = licenses.gpl3;
    platforms = platforms.unix;
    broken = false;
  };
}
