{ pkgs, stdenv, fetchgit, openmpi, automake, clang, openblas, ninja, simgrid }:

stdenv.mkDerivation rec {
  name = "gemmpi-${version}";
  version = "dev";

  src = fetchgit {
    url = "https://gitlab.inria.fr/adfaure/gemmpi";
    rev = "f207addeed2c09b25e4798272856a95717cdec53";
    sha256 = "sha256-/VWcNQrdCv7m4PtTdDB5Bu3wsLVtN2FL5nOGrt5L4h4=";
  };

  nativeBuildInputs = [ clang openblas ninja ];

  buildInputs = [ openmpi simgrid pkgs.boost ];

  buildPhase = ''
    mpicc --version
    mpirun --version
    ninja
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp -r gemmpi   $out/bin/
    cp -r igemmpi   $out/bin/
    cp -r gemsmpi  $out/bin/
    cp -r senrec.mpi $out/bin/
    cp -r pdgemm_stress $out/bin/
    # cp -r gemsmpi_tcpkali $out/bin/
  '';

  meta = with stdenv.lib; {
    longDescription = ''
      Performs distributed pdgemm, the algorithm is the direct apadtation of the outer product
      discribed in Parallel Algorithms (Chapman & Hall/CRC Numerical Analysis and Scientific Computing Series).
    '';
    description = ''
      Matrix multiplication benchmark on MPI.
    '';
    homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
    license = licenses.gpl3;
    platforms = platforms.unix;
    broken = false;
  };

}
