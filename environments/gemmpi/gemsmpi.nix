{ pkgs, stdenv, fetchgit, openmpi, automake, clang, openblas, ninja, simgrid, pkgconfig }:

stdenv.mkDerivation rec {
  name = "gemmpi-${version}";
  version = "dev-1";

  src = fetchgit {
    url = "https://gitlab.inria.fr/adfaure/gemmpi";
    rev = "97aad5afc27502b9a2fa7093023f87adbfa045ef";
    sha256 = "sha256-aCPe1kUoMI1onS+uuWQTezBGPOfNnhUGgakW0AY6ZPI=";
  };

  # src = /home/adfaure/Projects/gemmpi;

  nativeBuildInputs = [ clang ninja pkgconfig ];

  buildInputs = [ openmpi simgrid pkgs.boost openblas ];

  buildPhase = ''
    mkdir -p $out/bin
    mpicc --version
    mpirun --version
    ls -l ${simgrid}
    echo ${simgrid}
    smpicc -DGEMSMPI src/main.c -o gemsmpi  $(pkg-config --libs --cflags cblas) -lm
  '';

  installPhase = ''
    cp -r gemsmpi $out/bin/
    # cp -r igemmpi $out/bin/
    # cp -r gemsmpi $out/bin/,
  '';

  meta = with stdenv.lib; {
    longDescription = ''
      Performs distributed pdgemm, the algorithm is the direct apadtation of the outer product
      discribed in Parallel Algorithms (Chapman & Hall/CRC Numerical Analysis and Scientific Computing Series).
    '';
    description = ''
      Matrix multiplication benchmark on MPI.
    '';
    homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
    license = licenses.gpl3;
    platforms = platforms.unix;
    broken = false;
  };

}
