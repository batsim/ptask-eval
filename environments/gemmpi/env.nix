{ pkgs, stdenv, openmpi, gemmpi, simgrid, mojitos, ... }:
with pkgs;
let
  eztrace = pkgs.stdenv.mkDerivation {
    name = "ezTrace";
    src = fetchgit {
      url = "https://scm.gforge.inria.fr/anonscm/git/eztrace/eztrace.git";
      rev = "c1a5a14dc411e44218425906eefa6d3680c5be8f";
      sha256 = "sha256-/VWcNQrdCv7m4PtTdDB5Bu3wsLVtN2FL5nOGrt5L4h4=";
    };
    preConfigure = "./bootstrap";
    configureFlags = [
      "--with-mpi=${pkgs.openmpi}"
    ];
    buildInputs = with pkgs; [
      automake
      autoconf
      libtool
      libelf
      libbfd
      gfortran
      libopcodes
      openmpi
    ];
  };

in
pkgs.buildEnv {
  name = "gemmpiEnv";

  paths = with pkgs; [
    simgrid
    openmpi
    gemmpi
    openblas
    eztrace
    mojitos
  ];

   meta = with stdenv.lib; {
    description = ''
      Experiment environment for gemmpi with Simgrid.
    '';
    longDescription = ''
      Experiment environment for gemmpi with Simgrid.
      Contains Simgrid and Mpi.
    '';
    homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
    license = licenses.gpl3;
    platforms = platforms.unix;
    broken = false;
  };

}
