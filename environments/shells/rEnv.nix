{ pkgs }:

let
  r_pkgs = with pkgs.rPackages; [
    # add r packages here
    tidyverse
    viridis

    # Confidence interval
    Rmisc

    ggpubr
    bookdown
    devtools
    ggrepel
    yaml
    knitr
    rmarkdown
    plotly
    reshape
    reshape2
    lubridate
    servr
    hms
    xlsx

    # Required for running python in knitr
    reticulate
    # rhdf5
    # h5
    interval
    fuzzyjoin
    # required for knitr export
    caTools
    bitops
  ];

  # Script to transform the traces of gemmpi ranks into a csv summing up all phases
  my_r_scripts = pkgs.stdenv.mkDerivation {
    name = "scripts";
    propagatedBuildInputs = with pkgs.rPackages; [ tidyverse viridis ];
    installPhase = ''
      mkdir -p $out/bin
      cp -r scripts/* $out/bin
    '';
    src = ./../../analysis;
  };

in
pkgs.mkShell {
  buildInputs = with pkgs; [
    # simgrid
    pandoc

    #(python3.withPackages(ps: with ps; [
    python3
    python3Packages.pandas
    python3Packages.ipython
    python3Packages.aiofiles
    python3Packages.pandas
    python3Packages.numpy
    python3Packages.pyyaml
    python3Packages.matplotlib
    #]))

    my_r_scripts

    pkgs.unzip
    pkgs.gnutar
    # pkgs.hdf5

    (pkgs.rWrapper.override {
      packages = r_pkgs;
    })

    (pkgs.rstudioWrapper.override {
      packages = with pkgs.rPackages; [ viridis tidyverse reticulate ];
    })

  ];
}
