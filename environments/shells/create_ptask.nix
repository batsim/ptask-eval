{ kapack ? import
  # last working commit: f70b1149a9ae30fafe87af46e517985bacc331c3
  (fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz") { }
, pkgs ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/refs/tags/21.05.tar.gz") { }, gemmpi
, nur-kapack ? import
  (fetchTarball "https://github.com/oar-team/nur-kapack/archive/master.tar.gz")
  { },
  openblas,
  simgrid,
  sg_simulation,
  #  exptools ? import ~/Projects/exptools { pkgs = pkgs; }
  exptools ? import (fetchTarball {
    url =
      "https://gitlab.inria.fr/adfaure/ptasktools/-/archive/master/ptasktools-master.tar.gz";
    }) { pkgs = pkgs; }
}:

let

in rec {
  experiment_env = pkgs.mkShell rec {
    name = "experiment_env";
    buildInputs = with pkgs; [
      # simulator
      # kapack.pajeng
      sg_simulation.buildInputs

      gemmpi
      simgrid

      # Data analysis
      python3Packages.ipython
      python3Packages.aiofiles
      python3Packages.pandas
      python3Packages.numpy
      python3Packages.ruamel_yaml
      python3Packages.matplotlib
      python3Packages.pyyaml
      python3Packages.black
      # python3Packages. # python37Packages.ortools

      (pkgs.rstudioWrapper.override {
        packages = with rPackages; [ tidyverse viridis reticulate ];
      })
      exptools
    ];
  };
}
