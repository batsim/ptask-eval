{ simgrid, kapack, pkgs, gemmpi }:

with kapack;
let

  jupyter = import (builtins.fetchGit {
    url = https://github.com/tweag/jupyterWith;
    rev = "21da512ee016b8374033eea7772e77cc9e21158e";
  }) {};

  # Script to transform the traces of gemmpi ranks into a csv summing up all phases
  my_r_scripts = pkgs.stdenv.mkDerivation {
    name = "scripts";
    propagatedBuildInputs = with pkgs.rPackages; [ tidyverse viridis ];
    installPhase = ''
      mkdir -p $out/bin
      cp -r scripts/* $out/bin
    '';
    src = ./../../analysis;
  };

  exptools = import (fetchTarball {
    url = https://gitlab.inria.fr/adfaure/ptasktools/-/archive/master/ptasktools-master.tar.gz;
  }) {
    inherit pkgs;
    # pythonPackages = pkgs.python3Packages;
  };

  kaPython = jupyter.kernels.iPythonWith {
    name = "kapython";
    python3 = pkgs.python3;
    packages = p: with p; [
      ipython
      aiofiles
      pandas
      numpy
      ruamel_yaml
      matplotlib
      pyyaml

      # rpy2 + deps
      exptools

      tzlocal
      simplegeneric
      # (rpy2.overrideAttrs(attrs : {
      #   buildInputs = attrs.buildInputs ++ (with pkgs.rPackages; [ tidyverse viridis xtable extrafont ]);
      # }))
    ];
  };

  jupyterEnvironment =
    jupyter.jupyterlabWith {
      kernels = [
        kaPython
      ];
  };

in
pkgs.mkShell {
   buildInputs = with pkgs; [
      (pkgs.rstudioWrapper.override {
        packages = with rPackages; [tidyverse viridis reticulate];
      })
      (pkgs.rWrapper.override {
        packages = with rPackages; [tidyverse viridis];
      })

      (python3.withPackages(ps: with ps; [
        pandas
        ipython
        aiofiles
        pandas
        numpy
        pyyaml
        matplotlib
      ]))

      my_r_scripts
      simgrid
      gemmpi
      kapack.pajeng
      # kapack.batsim
      # kapack.batsched
      # kapack.batexpe
      jupyterEnvironment
    ];
}
