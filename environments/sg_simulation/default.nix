{ pkgs, stdenv, fetchgit, simgrid, pkgconfig, meson, ninja, rapidjson, docopt_cpp, boost }:

stdenv.mkDerivation rec {
  name = "gemmpi-${version}";
  version = "dev-1";

  src = pkgs.lib.sourceByRegex ../../experiments/simgrid [
        "^src"
        "^src/main.cpp"
        "^meson.build"
  ];

  buildInputs = [ ninja meson simgrid rapidjson docopt_cpp pkgconfig boost ];

  mesonBuildType = "debug";

  meta = with stdenv.lib; {
    longDescription = ''
      Performs distributed pdgemm, the algorithm is the direct apadtation of the outer product
      discribed in Parallel Algorithms (Chapman & Hall/CRC Numerical Analysis and Scientific Computing Series).
    '';
    description = ''
      Matrix multiplication benchmark on MPI.
    '';
    homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
    license = licenses.gpl3;
    platforms = platforms.unix;
    broken = false;
  };
}