{ hostPkgs ? import (fetchTarball {
  name = "pkgs";
  url = "https://github.com/NixOS/nixpkgs/archive/21.05.tar.gz";
  sha256 = "sha256:1ckzhh24mgz6jd1xhfgx0i9mijk6xjqxwsshnvq789xsavrmsc36";
}) { },
# Last working: f70b1149a9ae30fafe87af46e517985bacc331c3
kapack ? import (fetchTarball {
  name = "kapack";
  url =
    "https://github.com/oar-team/kapack/archive/f70b1149a9ae30fafe87af46e517985bacc331c3.tar.gz";
  sha256 = "sha256:007xyvrcfl9qfi7947kv3clbb7wjniicn1ks6wf2dlcpd2qzmh22";
}) { }, nur-kapack ? import (fetchTarball {
  url =
    "https://github.com/oar-team/nur-kapack/archive/a358e6274b1159427638ab8197a7ec672a03b436.tar.gz";
  sha256 = "sha256:0qril85w591gdq1bmzh3jirwzvzp66icrcgsd5njggkdkqprvc9j";
}) { pkgs = hostPkgs; } }:
with nur-kapack;
let
  environments = rec {

    pkgs = hostPkgs;
    # gemmpi_dev = gemmpi.overrideDerivation (oldAttrs: {
    #   src = /home/afaure/Projects/colmet_book/experiments/gemmpi;
    # });

    # Python envs for jupyter
    kernelEnv = pkgs.callPackage ./shells/jupyter-kernels.nix {
      kapack = kapack;
      simgrid = simgrid-330light; # Potentially not the good version QQ
      gemmpi = gemsmpi;
      pkgs = hostPkgs;
    };

    simgrid-bmf = nur-kapack.simgrid-330light.overrideDerivation (oldAttrs: {
      buildInputs = oldAttrs.buildInputs ++ [ pkgs.eigen ];
      doCheck = false;
      name = "simgrid-bmf";
      src = pkgs.fetchFromGitLab {
        domain = "framagit.org";
        owner = "bruno.donassolo";
        repo = "simgrid";
        rev = "bmf_rework";
        sha256 = "sha256-2LPy0EXo7O+cGJ5DyW59lj2qCYzWOP+Ic/rMKwHqVP0=";
      };
    });

    rEnv = pkgs.callPackage ./shells/rEnv.nix { pkgs = hostPkgs; };

    # Package to build gemmpi with smpicc
    gemsmpi = pkgs.callPackage ./gemmpi/gemsmpi.nix {
      openmpi = kapack.openmpi;
      simgrid =  nur-kapack.simgrid-330;
    };

    # Dedicated shell to create ptasks from real executions
    create_ptask_shell = pkgs.callPackage ./shells/create_ptask.nix {
      gemmpi = gemsmpi;
      simgrid = simgrid-bmf;
      inherit sg_simulation;
    };

    sg_simulation = pkgs.callPackage ./sg_simulation {
      simgrid = simgrid-bmf;
      docopt_cpp = kapack.docopt_cpp;
    };

  };
in environments

