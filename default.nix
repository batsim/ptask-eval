# This shell defines a development environment for the Chord project.
{
  env ? import ./environments {}
}:
let
in
  env
