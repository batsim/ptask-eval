library(tidyverse)
library(Rmisc)
library(viridis)

conflicts()

grisou_path_50sdiv = "../experiments/g5k/data/grisou/12_july/80K_256ranks_1bcast_50subdiv/"
paravance_path_50sdiv = "../experiments/g5k/data/paravance/paravance_july_run1_13_july/80K_256ranks_1bcast_50subdiv/"

ilevels = c("No interference",
            "15s interference / 45s idle", 
            "15s interference / 15s idle",  
            "30s interference / 30s idle",
            "45s interference / 15s idle", 
            "Constant interference")

get_instance_folder = function(path, index) {
  dirs = list.dirs(path = path, full.names = TRUE, recursive = FALSE)
  return(paste0(dirs[index],"/"))
}

load_instance_grisou = function(path, noise) {
  instance = path
  data = read_table2(paste0(instance, "grisou-1.nancy.grid5000.fr.eno1.mojitos.csv")) %>% 
    dplyr::rename(timestamp = "#timestamp") %>% mutate(src = "Router (eno1)") %>% 
    bind_rows( read_table2(paste0(instance, "grisou-1.nancy.grid5000.fr.eno2.mojitos.csv")) %>%
                 dplyr::rename(timestamp = "#timestamp") %>%  mutate(src = "Router (eno2)") ) %>% 
    bind_rows( read_table2(paste0(instance, "grisou-10.nancy.grid5000.fr.eno2.mojitos.csv")) %>%
                 dplyr::rename(timestamp = "#timestamp") %>%  mutate(src = "Tcpkali") ) %>% 
    bind_rows( read_table2(paste0(instance, "grisou-11.nancy.grid5000.fr.eno2.mojitos.csv")) %>%
                 dplyr::rename(timestamp = "#timestamp") %>% mutate(src = "MPI (only one host)") ) %>%
    mutate(noise = noise, timestamp = timestamp - min(timestamp))
}


load_instance_paravance = function(path, noise) {
  instance = path
  data = read_table2(paste0(instance, "paravance-1.rennes.grid5000.fr.eno1.mojitos.csv")) %>% 
    dplyr::rename(timestamp = "#timestamp") %>% mutate(src = "Router (eno1)") %>% 
    bind_rows( read_table2(paste0(instance, "paravance-1.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 dplyr::rename(timestamp = "#timestamp") %>%  mutate(src = "Router (eno2)")) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-10.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 dplyr::rename(timestamp = "#timestamp") %>%  mutate(src = "Tcpkali") ) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-11.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 dplyr::rename(timestamp = "#timestamp") %>% mutate(src = "MPI (only one host)") ) %>%
    mutate(noise = noise, timestamp = timestamp - min(timestamp))
}

grisou_bcast50_no_inter     = load_instance_grisou(get_instance_folder(paste0(grisou_path_50sdiv, "256c_no_kali_bcast"), 1), "No interference")
grisou_bcast50_full_inter   = load_instance_grisou(get_instance_folder(paste0(grisou_path_50sdiv, "256c_noper_bcast"), 1), "Constant interference")
grisou_bcast50_fifteen_inter    = load_instance_grisou(get_instance_folder(paste0(grisou_path_50sdiv, "256c_per15s_bcast"), 1), "15s interference / 15s idle")
grisou_bcast50_thirty_inter = load_instance_grisou(get_instance_folder(paste0(grisou_path_50sdiv, "256c_periods30s_bcast"), 1), "30s interference / 30s idle")
grisou_bcast50_idle45if15 = load_instance_grisou(get_instance_folder(paste0(grisou_path_50sdiv, "256c_per45sidle15inter_bcast"), 1), "15s interference / 45s idle")
grisou_bcast50_idle15if45 = load_instance_grisou(get_instance_folder(paste0(grisou_path_50sdiv, "256c_per15sidle45sinter_bcast"), 1), "45s interference / 15s idle")

grisou <- bind_rows(grisou_bcast50_full_inter, list(grisou_bcast50_no_inter, grisou_bcast50_fifteen_inter, grisou_bcast50_thirty_inter, grisou_bcast50_idle45if15 , grisou_bcast50_idle15if45)) %>%
  mutate(noise = factor(noise, levels = ilevels), cluster = "Grisou")

paravance_bcast50_no_inter     = load_instance_paravance(get_instance_folder(paste0(paravance_path_50sdiv, "256c_no_kali_bcast"), 1), "No interference")
paravance_bcast50_full_inter   = load_instance_paravance(get_instance_folder(paste0(paravance_path_50sdiv, "256c_noper_bcast"), 1), "Constant interference")
paravance_bcast50_fifteen_inter    = load_instance_paravance(get_instance_folder(paste0(paravance_path_50sdiv, "256c_per15s_bcast"), 1), "15s interference / 15s idle")
paravance_bcast50_thirty_inter = load_instance_paravance(get_instance_folder(paste0(paravance_path_50sdiv, "256c_periods30s_bcast"), 1), "30s interference / 30s idle")
paravance_bcast50_idle45if15 = load_instance_paravance(get_instance_folder(paste0(paravance_path_50sdiv, "256c_per45sidle15inter_bcast"), 1), "15s interference / 45s idle")
paravance_bcast50_idle15if45 = load_instance_paravance(get_instance_folder(paste0(paravance_path_50sdiv, "256c_per15sidle45sinter_bcast"), 1), "45s interference / 15s idle")

paravance <- bind_rows(paravance_bcast50_full_inter, list(paravance_bcast50_no_inter, paravance_bcast50_fifteen_inter, paravance_bcast50_thirty_inter, paravance_bcast50_idle45if15 , paravance_bcast50_idle15if45)) %>%
  mutate(noise = factor(noise, levels = ilevels), cluster = "Paravance")

both_clusters = bind_rows(grisou, paravance)

# Plot for my thesis
both_clusters %>% 
  filter(noise != "15s interference / 15s idle", noise != "15s interference / 45s idle", noise != "45s interference / 15s idle") %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) +

  ylab("transferred bytes (bytes/s)") + xlab("Time (s)") +
  geom_line() +
  facet_grid(noise~cluster) +
  theme_minimal(base_size = 18) +
  scale_colour_viridis_d(end = 0.7) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_grisou_comparison.pdf", height = 9, width = 10)

grisou_data = read_csv("../experiments/g5k/data/grisou/12_july/grisou_all_instance.csv") %>% 
  mutate(interference = str_replace(interference, "no_kali", "No interference")) %>%
  mutate(interference = str_replace(interference, "noper", "Constant interference")) %>%
  mutate(interference = str_replace(interference, "per15sidle45sinter", "45s interference / 15s idle")) %>%
  mutate(interference = str_replace(interference, "per45sidle15inter", "15s interference / 45s idle")) %>%
  mutate(interference = str_replace(interference, "per45sidle15sinter", "15s interference / 45s idle")) %>%
  mutate(interference = str_replace(interference, "per15s", "15s interference / 15s idle")) %>%
  mutate(interference = str_replace(interference, "periods30s", "30s interference / 30s idle")) %>%
  mutate(nb_sub = str_replace(nb_sub, "1_subdivision", "No subdivision"), 
         nb_sub = str_replace(nb_sub, "50_subdivision", "50 subdivisions")) %>%
  mutate(nb_sub = factor(nb_sub, levels = c("No subdivision", "50 subdivisions"))) %>% 
  mutate(bcast = str_replace(bcast, "IBcast", "Immediate broadcast"),
         bcast = str_replace(bcast, "Bcast", "Blocking broadcast")) %>%
  mutate(cluster = "Grisou")

paravance_data = read_csv("../experiments/g5k/data/paravance/paravance_july_run3_18_july/paravance_all_instance.csv") %>% 
  mutate(interference = str_replace(interference, "no_kali", "No interference")) %>%
  mutate(interference = str_replace(interference, "noper", "Constant interference")) %>%
  mutate(interference = str_replace(interference, "per15sidle45sinter", "45s interference / 15s idle")) %>%
  mutate(interference = str_replace(interference, "per45sidle15inter", "15s interference / 45s idle")) %>%
  mutate(interference = str_replace(interference, "per45sidle15sinter", "15s interference / 45s idle")) %>%
  mutate(interference = str_replace(interference, "per15s", "15s interference / 15s idle")) %>%
  mutate(interference = str_replace(interference, "periods30s", "30s interference / 30s idle")) %>%
  mutate(nb_sub = str_replace(nb_sub, "1_subdivision", "No subdivision"), 
         nb_sub = str_replace(nb_sub, "50_subdivision", "50 subdivisions")) %>%
  mutate(nb_sub = factor(nb_sub, levels = c("No subdivision", "50 subdivisions"))) %>% 
  mutate(bcast = str_replace(bcast, "IBcast", "Immediate broadcast"),
         bcast = str_replace(bcast, "Bcast", "Blocking broadcast")) %>%
  mutate(cluster = "paravance")

clusters_data = bind_rows(paravance_data, grisou_data)

clusters_data %>% ggplot(aes(y = runtime, x = factor(interference, levels = ilevels), color = nb_sub)) +
  geom_point(stat = "identity", position = position_dodge(), alpha = 0.7)+  
  xlab("Interference pattern") + ylab("Application running time (s)") +
  facet_grid(bcast~cluster) + 
  scale_color_viridis_d(end = 0.6) + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
  theme_bw(base_size = 18) +
  theme(legend.position = "bottom") + 
  # coord_flip() +
  ggsave("img/paravance_pdgemm_real_runtimes.pdf", width = 8, height = 7)


## Plot for my defense thesis
defense_levels = c("Interférences:\n0 %",
                   "15s interference / 45s idle", 
                   "15s interference / 15s idle",  
                   "Interférences:\n50 %",
                   "45s interference / 15s idle", 
                   "Interférences:\n100 %")

both_clusters %>% 
  filter(noise != "15s interference / 15s idle", noise != "15s interference / 45s idle", noise != "45s interference / 15s idle") %>%
  mutate(noise = str_replace(noise, "No interference", "Interférences:\n0 %")) %>%
  mutate(noise = str_replace(noise, "30s interference / 30s idle", "Interférences:\n50 %")) %>%
  mutate(noise = str_replace(noise, "Constant interference", "Interférences:\n100 %")) %>%
  mutate(noise = factor(noise, levels = defense_levels)) %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) +
  ylab("Octets transférés (octets/s)") + xlab("Temps (s)") +
  geom_line(size = 0.6) +
  facet_grid(noise~cluster) +
  theme_minimal(base_size = 21) +
  # scale_colour_viridis_d(end = 0.7) +
  scale_color_manual(values= c("#35b779ff", "#31688e",  "#482275ff")) +
  theme(legend.position = "bottom") + 
  # theme(legend.position = c(.9, -0.1)) +
  ggsave("img/paravance_grisou_comparison.pdf", width = 9, height = 6.9) +
  xlim(0, 500) +
  ggsave("img/paravance_grisou_comparison_zoom.pdf", width = 9, height = 6.9)

