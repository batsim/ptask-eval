import yaml
import pprint
import glob
import subprocess
import logging
import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
import tarfile
import os, shutil, sys, tarfile
import numpy as np
from datetime import datetime, timedelta
from scipy.stats import sem, t
from scipy import mean

plt.rcParams["figure.figsize"] = (8, 6)


def extract_nonexisting(archive, directory):
    for name in archive.getnames():
        if os.path.exists(os.path.join(directory, name)):
            # print(name, "already exists")
            pass
        else:
            archive.extract(name, path=directory)


class ExperimentInstance:
    def __init__(self, instance_folder):
        self.instance_folder = instance_folder
        self.mojitos = dict()
        self.untar_test()
        self.load()
        if "reached_timeout" in self.out_data["mpi"]:
            self.timeout = "True" == self.out_data["mpi"]["reached_timeout"]
        else:
            self.timeout = None
        if self.in_data["run_mpi"] is True:
            # self.load_mojitos()
            if not self.timeout:
                self.gemmpi_progress()
            else:
                print(self.instance_folder + " has reached timeout")

    def load(self):
        in_data_file = os.path.join(self.instance_folder, "in.yaml")
        out_data_file = os.path.join(self.instance_folder, "out.yaml")
        with open(in_data_file, "r") as stream:
            try:
                self.in_data = yaml.load(stream, Loader=yaml.FullLoader)
            except yaml.YAMLError as exc:
                print("yamml error with: %s" % in_data_file)

        with open(out_data_file, "r") as stream:
            try:
                self.out_data = yaml.load(stream, Loader=yaml.FullLoader)
            except yaml.YAMLError as exc:
                print("yamml error with: %s" % out_data_file)

    def print_out_data(self):
        pprint.pprint(self.out_data)

    def print_in_data(self):
        pprint.pprint(self.out_data)

    def gemmpi_progress(self):
        logs = glob.glob(os.path.join(self.instance_folder, "mpi.progress.*"))
        logs.sort()
        filename = os.path.join(self.instance_folder, "mpi.progress.full")

        if not os.path.exists(filename):
            with open(filename, "w") as outfile:
                csv = "ignore,rank,action,action_event,loop,time\n"
                outfile.write(csv)

                for fname in logs:
                    with open(fname) as infile:
                        outfile.write(infile.read())

        res = os.path.join(self.instance_folder, "res")
        p = subprocess.run(
            ["application_progress.R", filename, res],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        # print(p.stdout)
        # print(p.stderr)

        self.mpi_progress = pd.read_csv(res)

        out = os.path.join(self.instance_folder, "mpi_progress.csv")
        self.mpi_progress.to_csv(out)
        return out

    def print_progress(self):
        self.mpi_progress.plot(y="loop", x="max_comp_end")

    def untar_test(self):
        directory = self.instance_folder
        archives = [name for name in os.listdir(directory) if name.endswith("tar")]
        for archive_name in archives:
            archive_path = os.path.join(self.instance_folder, archive_name)
            with tarfile.open(archive_path) as archive:
                extract_nonexisting(archive, directory)

    def untar_all(self):
        tars = glob.glob(os.path.join(self.instance_folder, "*.tar"))
        for tar in tars:
            p = subprocess.run(
                ["tar", "xvf", tar, "-C", self.instance_folder], stderr=subprocess.PIPE
            )
            if p.returncode > 0:
                print("fail to untar: %s" % tar)

    def load_mojitos(self):
        mpi_g1 = self.out_data["network"]["instance_hosts"]["g1"]
        mpi_g2 = self.out_data["network"]["instance_hosts"]["g2"]
        hosts = mpi_g1 + mpi_g2 + [self.out_data["network"]["net_g1"]["router"]["g5k"]]

        filename_tmpl = "{}.mojitos.csv"
        for host in hosts:
            mojitos = filename_tmpl.format(host)
            path = os.path.join(self.instance_folder, mojitos)
            if not os.path.isfile(path):
                logging.warning("file %s does not exist" % path)

            self.mojitos[host] = pd.read_csv(path, delim_whitespace=True)

    def plot_host(self, hostname):
        return self.mojitos[hostname].plot(x="#timestamp", y=["rxb", "txb"])

    def get_instance_name(self):
        return self.out_data["instance_name"]

    def get_run_id(self):
        return self.out_data["run_id"]

    def get_runtime(self):
        return self.out_data["mpi"]["runtime"]

    def get_runtime2(self):
        return max(self.mpi_progress["max_comp_end"]) - min(
            self.mpi_progress["min_bcast_begin"]
        )

    def clean_res(self):
        res = os.path.join(self.instance_folder, "res")
        os.remove(res)


class Instances:
    def __init__(self, instances_folder):
        self.instance_folder = instances_folder
        self.instances = dict()
        self.name = None
        instances_dirs = glob.glob(os.path.join(self.instance_folder, "*"))
        for instances_dir in instances_dirs:
            i = ExperimentInstance(instances_dir)
            if self.name is None:
                self.name = i.get_instance_name()
            self.instances[i.get_instance_name() + "_" + i.get_run_id()] = i

    def get_mean_runtime(self):
        runtimes = []
        for instance in self.instances:
            runtimes.append(self.instances[instance].get_runtime2())
            print(
                self.instances[instance].get_run_id(),
                self.instances[instance].get_runtime2(),
            )
        self.runtimes = runtimes
        self.mean_runtime = np.mean(runtimes)

        return self.mean_runtime
