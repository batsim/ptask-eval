#!/usr/bin/env Rscript
library(tidyverse);
library(viridis)

args = commandArgs(trailingOnly=TRUE)

state = args[1];
link  = args[2];
marks = args[3]
loops = args[4]
results_dir = args[5]
zoom_s = args[6]
zoom_e = args[7]

df_state = read.csv(state, header=F, strip.white=T);
names(df_state) = c("Type", "Rank", "Container", "Start", "End", "Duration", "Level", "State");

df_link = read.csv(link, header=F, strip.white=T)
names(df_link) = c("Type", "Level", "Container", "Start", "End", "Duration", "CommType", "Src", "Dst");

df_state = df_state[!(names(df_state) %in% c("Type","Container","Level"))];
df_state$Rank = as.numeric(gsub("rank-","",df_state$Rank));

df_link = read.csv(link, header=F, strip.white=T)
names(df_link) = c("Type", "Level", "Container", "Start", "End", "Duration", "CommType", "Src", "Dst");
df_link = df_link[!(names(df_link) %in% c("Type","Container","Level","CommType"))]
df_link$Src = as.numeric(gsub("rank-","",df_link$Src))
df_link$Dst = as.numeric(gsub("rank-","",df_link$Dst))

df_marks = read_csv(marks) %>% mutate(loop=as.factor(loop))
enter = df_marks %>% filter(action_event == "begin")
exit  = df_marks %>% filter(action_event == "end")

join = full_join(enter, exit, by=c("rank", "loop", "action"), suffix=c("_begin", "_end"))
join %>% write_csv("/tmp/test.csv")

df_loop_aggregated = read_csv(loops)

df_state = df_state %>% mutate(State = as.factor(State))

ggplot(data=df_state) + scale_fill_viridis(discrete = TRUE) +
    coord_cartesian(xlim = c(as.numeric(zoom_s), as.numeric(zoom_e))) +
    # geom_rect(aes(xmin=Start, xmax=End, ymin=Rank+0.25, ymax=Rank+0.75, fill=State), color="black", alpha=0.4) +
    geom_rect(data=df_loop_aggregated, aes(xmin=min_bcast_begin, xmax=min_comp_end, ymin=-0.5, ymax=max(df_state$Rank)+1.5,
        color=as.factor(loop)), 
        size=0.5, 
        alpha=0) +
    geom_rect(data=join, 
              aes(xmin=time_begin, xmax=time_end, ymin=rank, ymax=rank+1, fill=as.factor(action)),
              alpha = 0.4, color="black", size=0.5) + 
    geom_point(data=df_marks, aes(x=time, y=rank + 0.5), alpha=0.8, size=0.1) + 
    ggsave(paste0(results_dir, "/comms.png") ,width=10, height=4) + 
    geom_segment(data = df_link, aes(x = Start, y = Src+0.5, xend = End, yend = Dst+0.5), 
                 size=0.2,
                 arrow = arrow(length = unit(0.01, "npc"))) +
    ggsave(paste0(results_dir, "/p2p.png"))

#  + theme(legend.position = "none")
