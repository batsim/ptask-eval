# PDGEMM real executions plots
library(tidyverse)
library(viridis)
library(ggpubr)

# setwd("analysis/")
theme_set(theme_minimal(base_size = 18))

ilevels = c("No interference",
            "15s interference / 45s idle", 
            "15s interference / 15s idle",  
            "30s interference / 30s idle",
            "45s interference / 15s idle", 
            "Constant interference")


# Paravance SWITCH 1
load_instance_paravance2 = function(path, noise) {
  instance = path
  data = read_table2(paste0(instance, "paravance-1.rennes.grid5000.fr.eno1.mojitos.csv")) %>% 
    rename(timestamp = "#timestamp") %>% 
    mutate(src = "Router (eno1)", timestamp = timestamp - min(timestamp)) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-1.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 rename(timestamp = "#timestamp") %>%  
                 mutate(src = "Router (eno2)", timestamp = timestamp - min(timestamp)) ) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-10.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 rename(timestamp = "#timestamp") %>%  
                 mutate(src = "Tcpkali", timestamp = timestamp - min(timestamp)) ) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-11.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 rename(timestamp = "#timestamp") %>% 
                 mutate(src = "MPI (only one host)", timestamp = timestamp - min(timestamp)) ) %>%
    mutate(noise = noise)
}

load_instance_paravance = function(path, noise) {
  instance = path
  data = read_table2(paste0(instance, "paravance-1.rennes.grid5000.fr.eno1.mojitos.csv")) %>% 
    rename(timestamp = "#timestamp") %>% mutate(src = "Router (eno1)") %>% 
    bind_rows( read_table2(paste0(instance, "paravance-1.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 rename(timestamp = "#timestamp") %>%  mutate(src = "Router (eno2)")) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-10.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 rename(timestamp = "#timestamp") %>%  mutate(src = "Tcpkali") ) %>% 
    bind_rows( read_table2(paste0(instance, "paravance-11.rennes.grid5000.fr.eno2.mojitos.csv")) %>%
                 rename(timestamp = "#timestamp") %>% mutate(src = "MPI (only one host)") ) %>%
    mutate(noise = noise, timestamp = timestamp - min(timestamp))
}

get_instance_folder = function(path, index) {
  dirs = list.dirs(path = path, full.names = TRUE, recursive = FALSE)
  return(paste0(dirs[index],"/"))
}
##

theme_set(theme_minimal(base_size = 18))

##
# Print running times
##

data = read_csv("../experiments/g5k/data/paravance/paravance_july_run3_18_july/paravance_all_instance.csv") %>% 
  mutate(interference = str_replace(interference, "no_kali", "No interference")) %>%
  mutate(interference = str_replace(interference, "noper", "Constant interference")) %>%
  mutate(interference = str_replace(interference, "per15sidle45sinter", "45s interference / 15s idle")) %>%
  mutate(interference = str_replace(interference, "per45sidle15inter", "15s interference / 45s idle")) %>%
  mutate(interference = str_replace(interference, "per45sidle15sinter", "15s interference / 45s idle")) %>%
  mutate(interference = str_replace(interference, "per15s", "15s interference / 15s idle")) %>%
  mutate(interference = str_replace(interference, "periods30s", "30s interference / 30s idle")) %>%
  mutate(nb_sub = str_replace(nb_sub, "1_subdivision", "No subdivision"), 
         nb_sub = str_replace(nb_sub, "50_subdivision", "50 subdivisions")) %>%
  mutate(nb_sub = factor(nb_sub, levels = c("No subdivision", "50 subdivisions"))) %>% 
  mutate(bcast = str_replace(bcast, "IBcast", "Immediate broadcast"),
         bcast = str_replace(bcast, "Bcast", "Blocking broadcast"))


# We compute the increase percentage of interference compared to the run without interference
# - Mean runtime for each config
# - Find the config with the lowest (mean) runtime (should be the no interference)
# - Attach this runtime to the others means and compute the increase percentage ((mean_runtime - base) / base) * 100 
t = 1.96 # Correspond to 95% confidence
stats <- data %>% group_by(interference, nb_sub, bcast) %>% 
  dplyr::summarise( sd_time = sd(runtime), 
                    mean_runtime = mean(runtime), 
                    n = n()) %>% 
  dplyr::mutate( ci_max = mean_runtime +t * (sd_time / sqrt(n)),
                 ci_min = mean_runtime - t * (sd_time / sqrt(n)),
                 key = paste(bcast, nb_sub) )

sums = data %>% group_by(bcast, nb_sub, interference) %>%
  summarize(mean = mean(runtime), sd = sd(runtime))

find_base = sums %>% group_by(bcast, nb_sub) %>% summarise(base = min(mean))

inc_percentages = full_join(sums, find_base, by= c("nb_sub", "bcast")) %>% 
  mutate(inc_percentage = (mean - base ) / base * 100) # %>% filter(interference != "No interference")

stats %>%
  ggplot(aes(y = mean_runtime, group = key, x = factor(interference, levels = ilevels), color = bcast)) +
  geom_point(size = 2) +
  geom_line() +
  geom_errorbar(aes(ymin = ci_min, ymax = ci_max), width = 1, color = "black", alpha = 0.6) +
  facet_grid(~nb_sub) +
  expand_limits(x = 0, y = 0) +
  scale_color_viridis_d(end = 0.6) +
  theme_bw(base_size = 18) +
  ylab("Runtime") +
  xlab("Interference pattern") +
  theme(legend.position = "bottom") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  ggsave("img/paravance_pdgemm_real_runtimes_errorbar.pdf", width = 8, height = 6) +
  # ggsave("~/Projects/Thesis/img/paravance_pdgemm_real_runtimes_errorbar.pdf", width = 8, height = 6)

# Visualizations

stats %>% ggplot(aes(y = mean_runtime, x = factor(interference, levels = ilevels), fill = bcast)) +
  geom_bar(stat = "identity", position = position_dodge())+  
  xlab("Interference pattern") + ylab("Application running time (s)") +
  facet_grid(~nb_sub) + 
  geom_errorbar(aes(ymin = ci_min, ymax = ci_max), 
                position = position_dodge(0.9), width = 0.5) +
  scale_fill_viridis_d(end = 0.6) + theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
  theme_bw(base_size = 18) +
  theme(legend.position = "bottom")

data %>% ggplot(aes(y = runtime, x = factor(interference, levels = rev(ilevels)), color = nb_sub)) +
  geom_point(stat = "identity", position = position_dodge(), alpha = 0.7)+  
  xlab("Interference pattern") + ylab("Application running time (s)") +
  facet_wrap(~bcast, ncol =  1) + 
  scale_color_viridis_d(end = 0.6) + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
  theme_bw(base_size = 18) +
  theme(legend.position = "bottom") + 
  coord_flip() +
  ggsave("img/paravance_pdgemm_real_runtimes.pdf", width = 8, height = 6)

inc_percentages %>% 
  ggplot(aes(y = inc_percentage, x = factor(interference, levels = rev(ilevels)), color = bcast)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
  geom_point() +
  facet_grid(~nb_sub) +
  scale_color_viridis_d(end = 0.6) + 
  coord_flip() +
  theme_bw(base_size = 18) +
  ylab("runtime % increase (base is no interference)") + 
  xlab("Interference pattern") + 
  theme(legend.position = "bottom")

library(xtable)


print(xtable(inc_percentages, type = "latex"), file = "data/paravance_table.tex")
# Print confidence intervals


path_50sdiv = "../experiments/g5k/data/paravance/paravance_july_run1_13_july/80K_256ranks_1bcast_50subdiv/"
path_1sdiv  = "../experiments/g5k/data/paravance/paravance_july_run1_13_july/80K_256ranks_1bcast_1subdiv/"

# 1 subdiv - Bcast
s1bcast =    read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_no_kali_bcast/"), 1), "/mpi_progress.csv")) %>%
               mutate(noise = "No interference") %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_noper_bcast/"), 1), "mpi_progress.csv")) %>%
               mutate(noise = "Constant interference") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_per15s_bcast/"), 1), "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_periods30s_bcast/"), 1),"mpi_progress.csv")) %>%
               mutate(noise = "30s interference / 30s idle") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_per45sidle15inter_bcast/"), 1),"/mpi_progress.csv")) %>%
               mutate(noise = "45s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_per15sidle45sinter_bcast/"), 1), "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 45s idle") ) %>%
  mutate(src = "No subdivision", bcast =  "Blocking broadcast")  %>% mutate(progress = loop/max(loop)*100)

# 1 subdiv - Bcast
s1ibcast =    read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_no_kali_ibcast/"), 1), "/mpi_progress.csv")) %>%
  mutate(noise = "No interference") %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_noper_ibcast/"), 1), "mpi_progress.csv")) %>%
               mutate(noise = "Constant interference") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_per15s_ibcast/"), 1), "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_periods30s_ibcast/"), 1),"mpi_progress.csv")) %>%
               mutate(noise = "30s interference / 30s idle") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_per45sidle15sinter_ibcast/"), 1),"/mpi_progress.csv")) %>%
               mutate(noise = "45s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(get_instance_folder(paste0(path_1sdiv, "256c_per15sidle45sinter_ibcast/"), 1), "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 45s idle") ) %>%
  mutate(src = "No subdivision", bcast =  "Nonblocking broadcast")  %>% mutate(progress = loop/max(loop)*100)


s50bcast =  read_csv(paste0(path_50sdiv,"256c_no_kali_bcast/57f03f2e/", "mpi_progress.csv")) %>%
  mutate(noise = "No interference") %>%
  bind_rows(read_csv(paste0(path_50sdiv,"256c_noper_bcast/3584b3ce/",  "mpi_progress.csv")) %>%
              mutate(noise = "Constant interference") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_per15s_bcast/2d060ca8/", "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_periods30s_bcast/2c6c49ab/", "mpi_progress.csv")) %>%
               mutate(noise = "30s interference / 30s idle") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_periods30s_bcast/2c6c49ab/", "mpi_progress.csv")) %>%
               mutate(noise = "45s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_per45sidle15inter_bcast/10ecadfe/", "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 45s idle") ) %>%
  mutate(src = "50 subdivisions", bcast = "Blocking broadcast")  %>% mutate(progress = loop/max(loop)*100)

s50ibcast =  read_csv(paste0(path_50sdiv, "256c_no_kali_ibcast/36020e63/", "mpi_progress.csv")) %>%
  mutate(noise = "No interference") %>%
  bind_rows(read_csv(paste0(path_50sdiv,"256c_noper_ibcast/d94bcd8d/",  "mpi_progress.csv")) %>%
              mutate(noise = "Constant interference") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_per15s_ibcast/a16a3c9c/", "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_periods30s_ibcast/44e63a51/", "mpi_progress.csv")) %>%
               mutate(noise = "30s interference / 30s idle") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_per15sidle45sinter_ibcast/68007535/", "mpi_progress.csv")) %>%
               mutate(noise = "45s interference / 15s idle") ) %>%
  bind_rows( read_csv(paste0(path_50sdiv,"256c_per45sidle15sinter_ibcast/079f2375/", "mpi_progress.csv")) %>%
               mutate(noise = "15s interference / 45s idle") ) %>%
  mutate(src = "50 subdivisions", bcast = "Nonblocking broadcast")  %>% mutate(progress = loop/max(loop)*100)

test = bind_rows(s1bcast, list(s1ibcast, s50bcast, s50ibcast)) %>% 
  mutate(nb_sub = factor(src, levels = c("No subdivision", "50 subdivisions"))) %>%
  rename("Noise" = "noise") %>%
  mutate(Noise = factor(Noise, levels = ilevels))     

test %>% ggplot(aes(x = max_comp_end, y = progress)) +
  geom_line(aes(color = Noise)) + facet_grid(bcast~nb_sub) + 
  ylab("Application progress(%)") + xlab("Time (s)") +
  scale_colour_viridis_d() + 
  theme_bw(base_size = 12) +
  theme(legend.position = "bottom") +
  ggsave("img/paravance_pdgemm_real_progress.pdf", height = 5, width = 8)

ibcast50_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_no_kali_ibcast"), 1), "No interference")
ibcast50_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_noper_ibcast"), 1), "Constant interference")
ibcast50_fifteen_inter    = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per15s_ibcast"), 1), "15s interference / 15s idle")
ibcast50_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_periods30s_ibcast"), 1), "30s interference / 30s idle")
ibcast50_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per45sidle15sinter_ibcast"), 1), "15s interference / 45s idle")
ibcast50_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per15sidle45sinter_ibcast"), 1), "45s interference / 15s idle")

line <- bind_rows(ibcast50_full_inter, list(ibcast50_no_inter, ibcast50_fifteen_inter, ibcast50_thirty_inter, ibcast50_idle45if15 , ibcast50_idle15if45)) %>%
  mutate(noise = factor(noise, levels = ilevels)) %>%
  filter(src != "Router (eno1)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) + 
  ylab("transferred bytes (bytes/s)") + xlab("Time (s)") +
  geom_line() +
  facet_wrap(~noise, ncol = 2) +
  theme_minimal(base_size = 18) +
  scale_colour_viridis_d(end = 0.7) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_ibcast_50sdiv.pdf", height = 7, width = 10)
line

bcast50_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_no_kali_bcast"), 1), "No interference")
bcast50_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_noper_bcast"), 1), "Constant interference")
bcast50_fifteen_inter    = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per15s_bcast"), 1), "15s interference / 15s idle")
bcast50_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_periods30s_bcast"), 1), "30s interference / 30s idle")
bcast50_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per45sidle15inter_bcast"), 1), "15s interference / 45s idle")
bcast50_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per15sidle45sinter_bcast"), 1), "45s interference / 15s idle")

line <- bind_rows(ibcast50_full_inter, list(bcast50_no_inter, bcast50_fifteen_inter, bcast50_thirty_inter, bcast50_idle45if15 , bcast50_idle15if45)) %>%
  mutate(noise = factor(noise, levels = ilevels)) %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) + 
  ylab("transferred bytes (bytes/s)") + xlab("Time (s)") +
  geom_line() +
  facet_wrap(~noise, ncol = 2) +
  theme_minimal(base_size = 18) +
  scale_colour_viridis_d(end = 0.7) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_bcast_50sdiv.pdf", height = 7, width = 10)
line

ibcast0_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_no_kali_ibcast"), 1), "No interference")
ibcast0_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_noper_ibcast"), 1), "Constant interference")
ibcast0_fifteen_inter    = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15s_ibcast"), 1), "15s interference / 15s idle")
ibcast0_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_periods30s_ibcast"), 1), "30s interference / 30s idle")
ibcast0_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per45sidle15sinter_ibcast"), 1), "15s interference / 45s idle")
ibcast0_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15sidle45sinter_ibcast"), 1), "45s interference / 15s idle")

line <- bind_rows(ibcast0_full_inter, list(ibcast0_no_inter, ibcast0_fifteen_inter, ibcast0_thirty_inter, ibcast0_idle45if15 , ibcast0_idle15if45)) %>%
  mutate(noise = factor(noise, levels = ilevels)) %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) + 
  ylab("transferred bytes (bytes/s)") + xlab("Time (s)") +
  geom_line() +
  facet_wrap(~noise, ncol = 2) +
  theme_minimal(base_size = 18) +
  scale_colour_viridis_d(end = 0.7) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_ibcast_1sdiv.pdf", height = 7, width = 10)
line

bcast0_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_no_kali_bcast"), 1), "No interference")
bcast0_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_noper_bcast"), 1), "Constant interference")
bcast0_fifteen_inter    = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15s_bcast"), 1), "15s interference / 15s idle")
bcast0_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_periods30s_bcast"), 1), "30s interference / 30s idle")
bcast0_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per45sidle15inter_bcast"), 1), "15s interference / 45s idle")
bcast0_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15sidle45sinter_bcast"), 1), "45s interference / 15s idle")

line <- bind_rows(bcast0_full_inter, list(bcast0_no_inter, bcast0_fifteen_inter, bcast0_thirty_inter, bcast0_idle45if15 , bcast0_idle15if45)) %>%
  mutate(noise = factor(noise, levels = ilevels)) %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) + 
  ylab("transferred bytes (bytes/s)") + xlab("Time (s)") +
  geom_line() +
  facet_wrap(~noise, ncol = 2) +
  theme_minimal(base_size = 18) +
  scale_colour_viridis_d(end = 0.7) +
  theme(legend.position = "bottom")  + 
  ggsave("img/paravance_bcast_1sdiv.pdf", height = 7, width = 10)
line


## Plots for my defense thesis

defense_levels = c("Pas d'interférence (0 %)",
            "15s interference / 45s idle", 
            "15s interference / 15s idle",  
            "30s interférence / 30s idle  (50 %)",
            "45s interference / 15s idle", 
            "Interférences constantes (100 %)")


ibcast50_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_no_kali_ibcast"), 1), "Pas d'interférences (0 %)")
ibcast50_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_noper_ibcast"), 2), "Interférences constantes (100 %)")
ibcast50_fifteen_inter = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per15s_ibcast"), 1), "15s interference / 15s idle")
ibcast50_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_periods30s_ibcast"), 1), "30s interférences / 30s idle  (50 %)")
ibcast50_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per45sidle15sinter_ibcast"), 1), "15s interference / 45s idle")
ibcast50_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_50sdiv, "256c_per15sidle45sinter_ibcast"), 1), "45s interference / 15s idle")

line <- bind_rows(ibcast50_full_inter, list(ibcast50_no_inter, ibcast50_thirty_inter)) %>%
  mutate(noise = factor(noise, levels = defense_levels)) %>%
  filter(src != "Router (eno1)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) +  
  ylab("Octets transférés (octets/s)") + xlab("Temps (s)") +
  geom_line(size = 0.8) +
  facet_wrap(~noise, ncol = 1) +
  theme_minimal(base_size = 21) +
  scale_color_manual(values= c("#35b779ff", "#31688e",  "#482275ff")) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_ibcast_50sdiv_defense.pdf", width = 8, height = 6.4)
line

ibcast0_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_no_kali_ibcast"), 1), "Pas d'interférences (0 %)")
ibcast0_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_noper_ibcast"), 1), "Interférences constantes (100 %)")
ibcast0_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_periods30s_ibcast"), 1), "30s interférences / 30s idle  (50 %)")
ibcast0_fifteen_inter = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15s_ibcast"), 1), "15s interference / 15s idle")
ibcast0_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per45sidle15sinter_ibcast"), 1), "15s interference / 45s idle")
ibcast0_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15sidle45sinter_ibcast"), 1), "45s interference / 15s idle")

line <- bind_rows(ibcast0_full_inter, list(ibcast0_no_inter, ibcast0_thirty_inter)) %>%
  mutate(noise = factor(noise, levels = defense_levels)) %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) + 
  ylab("Octets transférés (octets/s)") + xlab("Temps (s)") +
  geom_line(size = 0.6) +
  facet_wrap(~noise, ncol = 1) +
  theme_minimal(base_size = 18) +
  scale_color_manual(values= c("#35b779ff", "#31688e",  "#482275ff")) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_ibcast_1sdiv_defense.pdf", width = 8, height = 6.4)
line

bcast0_no_inter     = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_no_kali_bcast"), 1), "Pas d'interférences (0 %)")
bcast0_full_inter   = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_noper_bcast"), 1), "Interférences constantes (100 %)")
bcast0_fifteen_inter    = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15s_bcast"), 1), "15s interference / 15s idle")
bcast0_thirty_inter = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_periods30s_bcast"), 1), "30s interférences / 30s idle  (50 %)")
bcast0_idle45if15 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per45sidle15inter_bcast"), 1), "15s interference / 45s idle")
bcast0_idle15if45 = load_instance_paravance(get_instance_folder(paste0(path_1sdiv, "256c_per15sidle45sinter_bcast"), 1), "45s interference / 15s idle")

line <- bind_rows(bcast0_full_inter, list(bcast0_no_inter, bcast0_thirty_inter)) %>%
  mutate(noise = factor(noise, levels = defense_levels)) %>%
  filter(src != "Router (eno2)") %>%
  ggplot(aes(x = timestamp, y = txb, colour = src)) + 
  ylab("Octets transférés (octets/s)") + xlab("Temps (s)") +
  geom_line(size = 0.6) +
  facet_wrap(~noise, ncol = 1) +
  theme_minimal(base_size = 18) +
  scale_color_manual(values= c("#35b779ff", "#31688e",  "#482275ff")) +
  theme(legend.position = "bottom") + 
  ggsave("img/paravance_bcast_1sdiv_defense.pdf", width = 8, height = 6.4)
line

