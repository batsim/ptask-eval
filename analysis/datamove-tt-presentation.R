# For importing and reading csv
library(readr)
# For binding rows and mutating
library(dplyr)
# For plotting
library(ggplot2)
# Better colors
library(viridis)
# For interactive render
# library(plotly)
# Randomize factors
library(forcats)

paravance = read_csv("../experiments/g5k/data/graphkali/paravance/05-27-21:19:25:28/summary.csv") %>% mutate(cluster  = "Paravance")
grisou = read_csv("../experiments/g5k/data/graphkali/grisou/05-26-21:23:50:07/summary.csv") %>% mutate(cluster  = "Grisou")

data = paravance %>% bind_rows(grisou) %>% mutate(graph = ifelse(is.na(graph), "No interferences", graph))


data %>%
  ggplot(aes(
    y = mpi_runtime,
    x = total_connection_tcp,
    color =  factor(nb_tcp_pair)) ) +
  geom_point() +
  facet_wrap(~cluster) +
  # 0-based graphs are better
  expand_limits(x = 0, y = 0) +
  scale_color_viridis_d(end = 1) +
  xlab("Number of TCP connections") + ylab("Mpi runtime") + guides(color=guide_legend(title="Number of \nhost pairs")) +
  theme_bw(base_size = 14) +
  ggsave("img/datamovett_nb_connections.pdf", height = 12, units = "cm")

data %>%
  ggplot(aes(y = mpi_runtime, x = nb_tcp_pair, color = factor(total_connection_tcp))) +
  geom_point() +
  facet_wrap(~cluster) +
  expand_limits(x = 0, y = 0) +
  scale_color_viridis_d(end = 1) +
  xlab("Number of host pairs") + ylab("Mpi runtime") + guides(color=guide_legend(title="Number of \nconnections")) +
  theme_bw(base_size = 14) +
  ggsave("img/datamovett_nb_pair.pdf", height = 12, units = "cm")


# Load data
paravance_kali = read_csv("../experiments/g5k/data/graphkali/paravance/05-27-21:19:25:28/tcpkali.csv")
join_paravance = left_join(paravance_kali, paravance) %>% mutate(cluster = "Paravance")

grisou_kali = read_csv("../experiments/g5k/data/graphkali/grisou/05-26-21:23:50:07/tcpkali.csv")
join_grisou = left_join(grisou_kali, grisou) %>% mutate(cluster = "Grisou")

# Sum bandwidth per connection
data = bind_rows(join_grisou, join_paravance) %>%
  # Headers are missleading, it is not Mbps but M*B*ps (Mo/s)
  mutate(total_net = `Client upload (Mbps)` + `Client Downloads (Mbps)` )

# group by pair of hosts
group = data %>%
  group_by(`Client host`, `Server Host`, uuid) %>%
  summarise(total_connection_on_pair = n(), sum_bandwidth_per_node = sum(total_net))

# Join everything with information on pair
data = data %>% inner_join(group, by = c("Client host", "Server Host", "uuid"))

data %>% mutate(uuid = fct_reorder(uuid, total_connection_tcp)) %>%
  ggplot(aes(y = total_net, x = total_connection_tcp, color = factor(total_connection_on_pair))) +
  geom_point(size = 1) + # geom_hline(yintercept = 1250) +
  facet_wrap(~cluster) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  theme_bw() + scale_color_viridis(discrete = TRUE, end = 0.8) + expand_limits(x = 0, y = 0) +
  # theme(legend.position = "bottom") + guides(color=guide_legend(nrow=2,byrow=TRUE))
  labs(y = "Total bandwidth (Mo/s)", x = "Number of connections", color = "Number of \nconnections \nper pair") +
  theme_bw(base_size = 14) +
  ggsave("img/datamovett_total_bw_nb_conn.pdf", height = 12, units = "cm")

data %>% mutate(uuid = fct_reorder(uuid, total_connection_tcp)) %>%
  ggplot(aes(y = total_net, x = total_connection_on_pair, color = factor(total_connection_tcp))) +
  geom_point(size = 1) + # geom_hline(yintercept = 1250) +
  facet_wrap(~cluster) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  theme_bw() + scale_color_viridis(discrete = TRUE, end = 0.8) + expand_limits(x = 0, y = 0) +
  #  + guides(color=guide_legend(nrow=2,byrow=TRUE))
  labs(y = "Total bandwidth (Mo/s)", x = "Number of connections per pair",  color = "Number of \nconnections") +
  theme_bw(base_size = 14) +
  ggsave("img/datamovett_total_bw_nb_pair.pdf", height = 12, units = "cm")


# PTASK comparison with reality (nb connection)

grisou = read_csv("../experiments/g5k/data/graphkali/grisou/05-26-21:23:50:07/summary.csv") %>% mutate(application_type = "real", model = "None (real-world data)") %>%
  select(application_type, total_connection_tcp, mpi_runtime, model) %>%
  rename("number_of_connections" = total_connection_tcp, "runtime" = mpi_runtime)

ptask = read_csv("../experiments/simgrid/number_of_connections.all.csv") %>% rename("runtime" = runtime_predicted)

data = ptask %>% bind_rows(grisou)

data %>%
  ggplot(aes(
    y = runtime,
    x = number_of_connections,
    color = application_type,
    shape = model) ) +
  geom_point() +
  # 0-based graphs are better
  expand_limits(x = 0, y = 0) +
  scale_color_viridis_d(end = 0.7) +
  xlab("Number of connections in noise") + ylab("Application runtime (s)") +
  guides(color=guide_legend(title="Application runs as"), shape = guide_legend("SimGrid model used")) +
  theme_bw(base_size = 14)   + 
  ggsave("img/datamovett_comparison.pdf", height = 12, units = "cm")
