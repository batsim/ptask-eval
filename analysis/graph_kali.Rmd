---
output:
  html_notebook:
    code_folding: hide
    toc: true
    toc_float: true
    toc_depth: 3
author: "Adrien Faure"
params:
  title: "Study of intereferences on the platforms Paravance and Grisou"
  csv_paravance: ../experiments/g5k/data/graphkali/paravance/05-27-21:19:25:28/summary.csv
  csv_grisou: ../experiments/g5k/data/graphkali/grisou/05-26-21:23:50:07/summary.csv
---

---
title: `r params$title`
---

```{r, message=FALSE, warning=FALSE, class.source = 'fold-hide'}
# For importing and reading csv
library(readr)
# For binding rows and mutating
library(dplyr)
# For plotting
library(ggplot2)
# Better colors
library(viridis)
# For interactive render
# library(plotly)
# Randomize factors
library(forcats)

knitr::opts_chunk$set(dpi = 200, out.width = "85%")
```

## Description

We try to (in)validate the Ptask model, especially its capability to predict application runtime when the application is subject to network interferences.
To do that, we compare the execution of a simple application (a distributed matrix multiplication) with network interference as a Ptask and in reality.

The preliminary data from my PhD, showed that the two G5K platforms, Grisou and Paravance, have different behavior regarding network interferences.
**With the same interferences, the MPI application is not impacted the same way in Grisou and Paravance.**


```{r, echo=FALSE, out.width="50%"}
knitr::include_graphics("img/ptask_comparison.svg")
```

*This figure is extracted from my thesis, thus the x-axis depicts a `%` of interferences according to a time-period. In this notebook, we only study the application under interference (corresponding to `100 % interference`.).*

## Hypothesis

The network sharing algorithms of each platform's switch are different.

## Setup of the experiment

A switch is divided in two groups of nodes, connected by one node used as the router (the routing  node).

- All traffic from group 1 to group 2 must pass by the routing node.
- One MPI application is executed on both group.
- Interferences are generated during the MPI execution; the interference are TCP connections from group 1 to group 2 trying to get as many bandwidth as possible.

```{r, echo=FALSE}
knitr::include_graphics("img/ptask_logical_network_setup.svg")
```

In the experiment from my thesis, the interferences are made by 100 TCP connections, from one single host (in group 1) to another host (in group 2) -- easy to do with tcpkali.
In this notebook, we used generated different kind of interferences by splitting the connections across different pair of hosts (always one in group 1 and the other on group 2).

Especially in each configurations we have different parameters combination:

- The total number of TCP connections;
- The number of hosts involved in the interferences.

The interferences can be represented as a simple bipartite graph:
```{r, echo=FALSE, out.width="65%" }
knitr::include_graphics("img/tcp_topology.svg")
```
In the example above, we have 9 TCP connections. 4 host pairs involved.

We have a code that is able to generate different combination of such graphs, this is how we explore different configuration where there is different combination of number of pair of host / total number of connections.

## MPI runtime with interference

First we look at the how the MPI runtime is impacted according to two parameters from the intereferences:

- The total number of TCP connections (independently of where they are located).
- The total number of pair of hosts involved.

```{r, message=FALSE, warning=FALSE, class.source = 'fold-hide'}
grisou = read_csv(params$csv_grisou) %>% mutate(cluster  = "Grisou")
paravance = read_csv(params$csv_paravance) %>% mutate(cluster  = "Paravance")

data = paravance %>% bind_rows(grisou) %>% mutate(graph = ifelse(is.na(graph), "No interferences", graph))
```


```{r}
data %>%
    ggplot(aes(
        y = mpi_runtime,
        x = total_connection_tcp,
        color =  factor(nb_tcp_pair)) ) +
    geom_point() +
    facet_wrap(~cluster) +
    # 0-based graphs are better
    expand_limits(x = 0, y = 0) +
    scale_color_viridis_d(end = 1) +
    xlab("Number of TCP connections") + ylab("Mpi runtime") + guides(color=guide_legend(title="Number of \nhost pairs")) +
    theme_bw()
```

```{r}
# Same graph, but we switch nb_tcp_pair and total_connection_tcp
data %>%
    ggplot(aes(y = mpi_runtime, x = nb_tcp_pair, color = factor(total_connection_tcp))) +
    geom_point() +
    facet_wrap(~cluster) +
    expand_limits(x = 0, y = 0) +
    scale_color_viridis_d(end = 1) +
    xlab("Number of host pairs") + ylab("Mpi runtime") + guides(color=guide_legend(title="Number of \nconnections")) +
    theme_bw()
```

**Observation**

- On Grisou the runtime of the MPI application is dependent of the total number of TCP connection;
- While on Paravance, it seems that this is the number of host pairs that is relevant.

*Note: The number of TCP connections and number of host pairs are not independant: To increase the number of host pairs one needs to increase the number of connections.*

## Tcp connections analysis

In this section, we look the details per tcp connection.
Each point of on the figure shows the total bandwidth (upload + download) taken by a **single** tcp connection.
The horizontal line shows the max bandwidth of the link (1.25Mo/s).

```{r, message=FALSE, warning=FALSE}
# Load data
paravance_kali = read_csv("../experiments/g5k/data/graphkali/paravance/05-27-21:19:25:28/tcpkali.csv")
join_paravance = left_join(paravance_kali, paravance) %>% mutate(cluster = "Paravance")

grisou_kali = read_csv("../experiments/g5k/data/graphkali/grisou/05-26-21:23:50:07/tcpkali.csv")
join_grisou = left_join(grisou_kali, grisou) %>% mutate(cluster = "Grisou")
```

```{r, message=FALSE, warning=FALSE, class.source = 'fold-show'}
# Sum bandwidth per connection
data = bind_rows(join_grisou, join_paravance) %>%
  # Headers are missleading, it is not Mbps but M*B*ps (Mo/s)
  mutate(total_net = `Client upload (Mbps)` + `Client Downloads (Mbps)` )

# group by pair of hosts
group = data %>%
  group_by(`Client host`, `Server Host`, uuid) %>%
  summarise(total_connection_on_pair = n(), sum_bandwidth_per_node = sum(total_net))

# Join everything with information on pair
data = data %>% inner_join(group, by = c("Client host", "Server Host", "uuid"))
```

```{r}
data %>% mutate(uuid = fct_reorder(uuid, total_connection_tcp)) %>%
  ggplot(aes(y = total_net, x = total_connection_tcp, color = factor(total_connection_on_pair))) +
  geom_point(size = 1) + geom_hline(yintercept = 1250) +
  facet_wrap(~cluster) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  theme_bw() + scale_color_viridis(discrete = TRUE, end = 0.8) + expand_limits(x = 0, y = 0) +
  # theme(legend.position = "bottom") + guides(color=guide_legend(nrow=2,byrow=TRUE))
  labs(y = "Total bandwidth (Mo/s)", x = "Number of connections", color = "Number of \nconnections \nper pair")
```

```{r}
data %>% mutate(uuid = fct_reorder(uuid, total_connection_tcp)) %>%
  ggplot(aes(y = total_net, x = total_connection_on_pair, color = factor(total_connection_tcp))) +
  geom_point(size = 1) + geom_hline(yintercept = 1250) +
  facet_wrap(~cluster) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  theme_bw() + scale_color_viridis(discrete = TRUE, end = 0.8) + expand_limits(x = 0, y = 0) +
  #  + guides(color=guide_legend(nrow=2,byrow=TRUE))
  labs(y = "Total bandwidth (Mo/s)", x = "Number of connections per pair",  color = "Number of \nconnections")
```

**Observations**

- On Grisou, more active TCP connections on the switch means less bandwidth per connection;
- On Paravance, more active connections on the same pair of host means less bandwidth per connection (on the corresponding pair).


## Total bandwidth per hosts

The last figures shows, for each instance (x-axis) the total bandwidth taken by every host used for the interference (one point is one host).

```{r, echo = FALSE}
data %>%
 ggplot(aes(y = sum_bandwidth_per_node, x = uuid, color = factor(as.numeric(as.factor(uuid)) %% 4) )) +
 geom_point() + geom_hline(yintercept = 1250) +
 facet_wrap(~cluster, ncol = 1) +
 expand_limits(x = 0, y = 0) +
 scale_color_viridis_d(end = 0.8) +
 ylab("Total bandwidth per host (Mo/s)") + xlab("Instance") +
 theme_bw() + theme(axis.text.x = element_blank(), axis.ticks.x = element_blank()) +
 theme(legend.position = "none")
```

**Observations**

- Paravance bandwidths are generally more stable (less variability);
- In Grisou some nodes manage to get a big bandwidth.

